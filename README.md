# Verdbrefaspilid
Verdbrefaspilid is a pet project that aims to digitize the popular icelandic board game.
The game is written in C# and uses windows forms api for representation.
As of now the game is still in development and not nearly finished.

# The Azure DevOps build system
Azure DevOps is used for build automation. With Azure DevOps you manage your project in a 
web browser. When logged in you simply click the project you want to manage (in this case 
the project **Verdbrefaspilid**). When the project has been opened, a manual build can be
run by navigating to **Pipelines** clicking **More** (three dots) on the desired pipeline 
and selecting **Run pipeline** to start the build.
To access the artifact created by the build, click on the resulting job, the artifact can
be found under the name **Artifacts:**

# Testing
The test cases will run automatically at the end of the build pipeline. Simply starting a 
new build will trigger all unit tests available.