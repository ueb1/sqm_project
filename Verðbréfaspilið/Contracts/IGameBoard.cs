﻿using Game.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Contracts
{
    public interface IGameBoard
    {
        void MovePiece(Player player, int nrOfCells);
        void RefreshBoard(Wallet wallet);
        void NextPlayer(int playerNr);
        void SetPlayerName(string name);
    }
}
