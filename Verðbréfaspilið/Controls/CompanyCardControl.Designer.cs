﻿namespace Game.Controls
{
    partial class CompanyCardControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picImage = new System.Windows.Forms.PictureBox();
            this.pnlInfo = new System.Windows.Forms.Panel();
            this.lblDividend4 = new System.Windows.Forms.Label();
            this.lblDividend3 = new System.Windows.Forms.Label();
            this.lblDividend2 = new System.Windows.Forms.Label();
            this.lblDividend1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblEmployeeCount = new System.Windows.Forms.Label();
            this.lblCost = new System.Windows.Forms.Label();
            this.pnlContent = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).BeginInit();
            this.pnlInfo.SuspendLayout();
            this.pnlContent.SuspendLayout();
            this.SuspendLayout();
            // 
            // picImage
            // 
            this.picImage.Image = global::Game.Properties.Resources.logoEllingsen;
            this.picImage.Location = new System.Drawing.Point(11, 11);
            this.picImage.Name = "picImage";
            this.picImage.Size = new System.Drawing.Size(196, 100);
            this.picImage.TabIndex = 0;
            this.picImage.TabStop = false;
            // 
            // pnlInfo
            // 
            this.pnlInfo.Controls.Add(this.lblDividend4);
            this.pnlInfo.Controls.Add(this.lblDividend3);
            this.pnlInfo.Controls.Add(this.lblDividend2);
            this.pnlInfo.Controls.Add(this.lblDividend1);
            this.pnlInfo.Controls.Add(this.label9);
            this.pnlInfo.Controls.Add(this.label8);
            this.pnlInfo.Controls.Add(this.label7);
            this.pnlInfo.Controls.Add(this.label6);
            this.pnlInfo.Controls.Add(this.label5);
            this.pnlInfo.Controls.Add(this.label4);
            this.pnlInfo.Controls.Add(this.label3);
            this.pnlInfo.Controls.Add(this.label2);
            this.pnlInfo.Controls.Add(this.lblEmployeeCount);
            this.pnlInfo.Controls.Add(this.lblCost);
            this.pnlInfo.Location = new System.Drawing.Point(11, 111);
            this.pnlInfo.Name = "pnlInfo";
            this.pnlInfo.Size = new System.Drawing.Size(196, 176);
            this.pnlInfo.TabIndex = 1;
            // 
            // lblDividend4
            // 
            this.lblDividend4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lblDividend4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDividend4.Location = new System.Drawing.Point(130, 143);
            this.lblDividend4.Margin = new System.Windows.Forms.Padding(0);
            this.lblDividend4.Name = "lblDividend4";
            this.lblDividend4.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblDividend4.Size = new System.Drawing.Size(66, 32);
            this.lblDividend4.TabIndex = 13;
            this.lblDividend4.Text = "1000";
            this.lblDividend4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDividend3
            // 
            this.lblDividend3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblDividend3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDividend3.Location = new System.Drawing.Point(130, 111);
            this.lblDividend3.Margin = new System.Windows.Forms.Padding(0);
            this.lblDividend3.Name = "lblDividend3";
            this.lblDividend3.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblDividend3.Size = new System.Drawing.Size(66, 32);
            this.lblDividend3.TabIndex = 12;
            this.lblDividend3.Text = "500";
            this.lblDividend3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDividend2
            // 
            this.lblDividend2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lblDividend2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDividend2.Location = new System.Drawing.Point(130, 79);
            this.lblDividend2.Margin = new System.Windows.Forms.Padding(0);
            this.lblDividend2.Name = "lblDividend2";
            this.lblDividend2.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblDividend2.Size = new System.Drawing.Size(66, 32);
            this.lblDividend2.TabIndex = 11;
            this.lblDividend2.Text = "200";
            this.lblDividend2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDividend1
            // 
            this.lblDividend1.BackColor = System.Drawing.Color.Yellow;
            this.lblDividend1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDividend1.Location = new System.Drawing.Point(130, 47);
            this.lblDividend1.Margin = new System.Windows.Forms.Padding(0);
            this.lblDividend1.Name = "lblDividend1";
            this.lblDividend1.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblDividend1.Size = new System.Drawing.Size(66, 32);
            this.lblDividend1.TabIndex = 10;
            this.lblDividend1.Text = "50";
            this.lblDividend1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(65, 143);
            this.label9.Margin = new System.Windows.Forms.Padding(0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 32);
            this.label9.TabIndex = 9;
            this.label9.Text = "Arður";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(65, 111);
            this.label8.Margin = new System.Windows.Forms.Padding(0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 32);
            this.label8.TabIndex = 8;
            this.label8.Text = "Arður";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(65, 79);
            this.label7.Margin = new System.Windows.Forms.Padding(0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 32);
            this.label7.TabIndex = 7;
            this.label7.Text = "Arður";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Yellow;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(65, 47);
            this.label6.Margin = new System.Windows.Forms.Padding(0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 32);
            this.label6.TabIndex = 6;
            this.label6.Text = "Arður";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(0, 143);
            this.label5.Margin = new System.Windows.Forms.Padding(0);
            this.label5.Name = "label5";
            this.label5.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Size = new System.Drawing.Size(65, 32);
            this.label5.TabIndex = 5;
            this.label5.Text = "100%";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(0, 111);
            this.label4.Margin = new System.Windows.Forms.Padding(0);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Size = new System.Drawing.Size(65, 32);
            this.label4.TabIndex = 4;
            this.label4.Text = "75%";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(0, 79);
            this.label3.Margin = new System.Windows.Forms.Padding(0);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Size = new System.Drawing.Size(65, 32);
            this.label3.TabIndex = 3;
            this.label3.Text = "50%";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Yellow;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(0, 47);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Size = new System.Drawing.Size(65, 32);
            this.label2.TabIndex = 2;
            this.label2.Text = "25%";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblEmployeeCount
            // 
            this.lblEmployeeCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmployeeCount.Location = new System.Drawing.Point(3, 26);
            this.lblEmployeeCount.Name = "lblEmployeeCount";
            this.lblEmployeeCount.Size = new System.Drawing.Size(190, 21);
            this.lblEmployeeCount.TabIndex = 1;
            this.lblEmployeeCount.Text = "Starfsmannafjöldi: 50";
            this.lblEmployeeCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCost
            // 
            this.lblCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCost.Location = new System.Drawing.Point(3, 3);
            this.lblCost.Name = "lblCost";
            this.lblCost.Size = new System.Drawing.Size(190, 23);
            this.lblCost.TabIndex = 0;
            this.lblCost.Text = "Nafnverð: 500";
            this.lblCost.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pnlContent
            // 
            this.pnlContent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.pnlContent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlContent.Controls.Add(this.pnlInfo);
            this.pnlContent.Controls.Add(this.picImage);
            this.pnlContent.Location = new System.Drawing.Point(0, 0);
            this.pnlContent.Name = "pnlContent";
            this.pnlContent.Size = new System.Drawing.Size(220, 300);
            this.pnlContent.TabIndex = 1;
            // 
            // CompanyCardControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlContent);
            this.Name = "CompanyCardControl";
            this.Size = new System.Drawing.Size(220, 300);
            this.Load += new System.EventHandler(this.CompanyCardDialog_Load);
            this.Click += new System.EventHandler(this.CompanyCardControl_Click);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.CompanyCardControl_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).EndInit();
            this.pnlInfo.ResumeLayout(false);
            this.pnlContent.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picImage;
        private System.Windows.Forms.Panel pnlInfo;
        private System.Windows.Forms.Label lblDividend4;
        private System.Windows.Forms.Label lblDividend3;
        private System.Windows.Forms.Label lblDividend2;
        private System.Windows.Forms.Label lblDividend1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblEmployeeCount;
        private System.Windows.Forms.Label lblCost;
        private System.Windows.Forms.Panel pnlContent;
    }
}
