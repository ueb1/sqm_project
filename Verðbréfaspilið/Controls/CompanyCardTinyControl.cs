﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Game.Entities.Cards;
using Game.Entities;
using Game.Dialogs;

namespace Game.Controls
{
    public partial class CompanyCardTinyControl : UserControl
    {
        public event EventHandler ControlClicked;

        private int sizeOfStack;
        private Company company;
        private int baseHeight;

        public Company Company
        {
            get
            {
                return company;
            }
        }

        public CompanyCardTinyControl(Company company, int sizeOfStack)
        {
            InitializeComponent();

            baseHeight = Height;
            this.company = company;
            SetStackSize(sizeOfStack);

            picImage.Image = company.ImageTiny;
            lblCost.Text = "Nafnverð: " + company.CostEachShare;
            lblEmployeeCount.Text = "Starfsmannafjöldi: " + (company.NrOfStaff / Constants.NR_COMPANY_SHARES);
            lblDividend1.Text = Constants.COMPANY_DIVIDENDS[(int)company.BusinessGroup, 0].ToString();
            lblDividend2.Text = Constants.COMPANY_DIVIDENDS[(int)company.BusinessGroup, 1].ToString();
            lblDividend3.Text = Constants.COMPANY_DIVIDENDS[(int)company.BusinessGroup, 2].ToString();
            lblDividend4.Text = Constants.COMPANY_DIVIDENDS[(int)company.BusinessGroup, 3].ToString();
            BackColor = company.CompanyColor;
            pnlCard.BackColor = company.CompanyColor;
        }

        public void SetStackSize(int stackSize)
        {
            sizeOfStack = stackSize;
            if (sizeOfStack > 0)
            {
                Visible = true;
                //if (sizeOfStack > 1)
                //{
                    int padDown = (sizeOfStack - 1) * 3;
                    Height = baseHeight + padDown;
                    pnlCard.Location = new Point(pnlCard.Location.X, padDown);
                //}
            }
            else
                Visible = false;
        }

        private void CompanyCardTinyControl_Paint(object sender, PaintEventArgs e)
        {
            for (int i = 1; i < sizeOfStack - 1; i++)
            {
                e.Graphics.DrawLine(Pens.Black, 0, i * 3, Width, i * 3);
            }
            ControlPaint.DrawBorder(e.Graphics,
            ((Control)sender).ClientRectangle,
            Color.Black,
            ButtonBorderStyle.Solid);
        }

        private void CompanyCardTinyControl_Load(object sender, EventArgs e)
        {
            registerControls(pnlCard.Controls);
        }

        private void pnlCard_Paint(object sender, PaintEventArgs e)
        {
            //e.Graphics.DrawLine(Pens.Black, 0, 0, 0, pnlCard.Height - 1);
            //e.Graphics.DrawLine(Pens.Black, pnlCard.Width - 1, 0, pnlCard.Width - 1, pnlCard.Height - 1);
            ControlPaint.DrawBorder(e.Graphics,
            ((Control)sender).ClientRectangle,
            Color.Black,
            ButtonBorderStyle.Solid);
        }

        private void CompanyCardTinyControl_Click(object sender, EventArgs e)
        {
            ControlClicked?.Invoke(this, EventArgs.Empty);
        }

        private void registerControls(ControlCollection controls)
        {
            for (int i = 0; i < controls.Count; i++)
            {
                if (controls[i].Controls.Count > 0)
                    registerControls(controls[i].Controls);

                controls[i].Click += new EventHandler(CompanyCardTinyControl_Click);
            }
        }
    }
}
