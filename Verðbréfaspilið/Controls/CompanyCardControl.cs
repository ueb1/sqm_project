﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Game.Entities.Cards;
using Game.Entities;

namespace Game.Controls
{
    public partial class CompanyCardControl : UserControl
    {
        public event EventHandler ControlClicked;

        public CompanyCardControl()
        {
            InitializeComponent();
        }

        public CompanyCardControl(Company company)
        {
            InitializeComponent();

            init(company);
        }

        public void init(Company company)
        {
            picImage.Image = company.ImageLarge;
            lblCost.Text = "Nafnverð: " + company.CostEachShare;
            lblEmployeeCount.Text = "Starfsmannafjöldi: " + (int)(company.NrOfStaff / Constants.NR_COMPANY_SHARES);
            lblDividend1.Text = Constants.COMPANY_DIVIDENDS[(int)company.BusinessGroup, 0].ToString();
            lblDividend2.Text = Constants.COMPANY_DIVIDENDS[(int)company.BusinessGroup, 1].ToString();
            lblDividend3.Text = Constants.COMPANY_DIVIDENDS[(int)company.BusinessGroup, 2].ToString();
            lblDividend4.Text = Constants.COMPANY_DIVIDENDS[(int)company.BusinessGroup, 3].ToString();
            pnlContent.BackColor = company.CompanyColor;

            this.Invalidate();
            this.Refresh();
        }

        private void CompanyCardControl_Click(object sender, EventArgs e)
        {
            if(ControlClicked != null) // We are not multi-threading so we are ok with this null check
                ControlClicked.Invoke(this, EventArgs.Empty);
        }

        private void CompanyCardDialog_Load(object sender, EventArgs e)
        {
            registerControls(pnlContent.Controls);
        }

        private void registerControls(ControlCollection controls)
        {
            for(int i=0; i<controls.Count; i++)
            {
                if (controls[i].Controls.Count > 0)
                    registerControls(controls[i].Controls);

                controls[i].Click += new EventHandler(CompanyCardControl_Click);
            }
        }

        private void CompanyCardControl_Paint(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics,
            ((Control)sender).ClientRectangle,
            Color.Black,
            ButtonBorderStyle.Solid);
        }

    }
}
