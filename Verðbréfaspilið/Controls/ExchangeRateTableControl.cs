﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Game.Entities.Cards;

namespace Game.Controls
{
    public partial class ExchangeRateTableControl : UserControl
    {
        public ExchangeRateTableControl()
        {
            InitializeComponent();
        }

        public ExchangeRateTableControl(ExchangeCard exchangeCard)
        {
            SetExchangeRate(exchangeCard);
        }

        public void SetExchangeRate(ExchangeCard exchangeCard)
        {
            lblGroupRate1.Text = exchangeCard.StockRates[0].ToString();
            lblGroupRate2.Text = exchangeCard.StockRates[1].ToString();
            lblGroupRate3.Text = exchangeCard.StockRates[2].ToString();
            lblGroupRate4.Text = exchangeCard.StockRates[3].ToString();
            lblGroupRate5.Text = exchangeCard.StockRates[4].ToString();
            lblGroupRate6.Text = exchangeCard.StockRates[5].ToString();

            lblFundGroupRate1.Text = exchangeCard.FundLetterRates[0] == 0 ? "-" : exchangeCard.FundLetterRates[0].ToString();
            lblFundGroupRate2.Text = exchangeCard.FundLetterRates[1] == 0 ? "-" : exchangeCard.FundLetterRates[1].ToString();

            lblInterestRates.Text = "Vextir: " + (int)(exchangeCard.InterestRate * 100) + "%";
        }
    }
}
