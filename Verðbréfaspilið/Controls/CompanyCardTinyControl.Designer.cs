﻿namespace Game.Controls
{
    partial class CompanyCardTinyControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblEmployeeCount = new System.Windows.Forms.Label();
            this.lblCost = new System.Windows.Forms.Label();
            this.pnlCard = new System.Windows.Forms.Panel();
            this.picImage = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblDividend1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblDividend2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblDividend3 = new System.Windows.Forms.Label();
            this.lblDividend4 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.pnlCard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).BeginInit();
            this.SuspendLayout();
            // 
            // lblEmployeeCount
            // 
            this.lblEmployeeCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 3F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmployeeCount.Location = new System.Drawing.Point(5, 40);
            this.lblEmployeeCount.Name = "lblEmployeeCount";
            this.lblEmployeeCount.Size = new System.Drawing.Size(54, 5);
            this.lblEmployeeCount.TabIndex = 3;
            this.lblEmployeeCount.Text = "Starfsmannafjöldi: 50";
            this.lblEmployeeCount.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblCost
            // 
            this.lblCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCost.Location = new System.Drawing.Point(5, 32);
            this.lblCost.Name = "lblCost";
            this.lblCost.Size = new System.Drawing.Size(54, 8);
            this.lblCost.TabIndex = 2;
            this.lblCost.Text = "Nafnverð: 500";
            this.lblCost.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pnlCard
            // 
            this.pnlCard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.pnlCard.Controls.Add(this.lblDividend4);
            this.pnlCard.Controls.Add(this.label11);
            this.pnlCard.Controls.Add(this.label12);
            this.pnlCard.Controls.Add(this.lblDividend3);
            this.pnlCard.Controls.Add(this.label8);
            this.pnlCard.Controls.Add(this.label7);
            this.pnlCard.Controls.Add(this.lblDividend2);
            this.pnlCard.Controls.Add(this.label5);
            this.pnlCard.Controls.Add(this.label4);
            this.pnlCard.Controls.Add(this.lblDividend1);
            this.pnlCard.Controls.Add(this.label1);
            this.pnlCard.Controls.Add(this.label2);
            this.pnlCard.Controls.Add(this.picImage);
            this.pnlCard.Controls.Add(this.lblEmployeeCount);
            this.pnlCard.Controls.Add(this.lblCost);
            this.pnlCard.Location = new System.Drawing.Point(0, 0);
            this.pnlCard.Margin = new System.Windows.Forms.Padding(0);
            this.pnlCard.Name = "pnlCard";
            this.pnlCard.Size = new System.Drawing.Size(64, 87);
            this.pnlCard.TabIndex = 1;
            this.pnlCard.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlCard_Paint);
            // 
            // picImage
            // 
            this.picImage.Image = global::Game.Properties.Resources.logoEllingsen_tiny;
            this.picImage.Location = new System.Drawing.Point(5, 5);
            this.picImage.Margin = new System.Windows.Forms.Padding(0);
            this.picImage.Name = "picImage";
            this.picImage.Size = new System.Drawing.Size(54, 27);
            this.picImage.TabIndex = 1;
            this.picImage.TabStop = false;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Yellow;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 3F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(5, 49);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.label2.Size = new System.Drawing.Size(18, 8);
            this.label2.TabIndex = 5;
            this.label2.Text = "25%";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Yellow;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 3F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 49);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.label1.Size = new System.Drawing.Size(18, 8);
            this.label1.TabIndex = 6;
            this.label1.Text = "Arður";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDividend1
            // 
            this.lblDividend1.BackColor = System.Drawing.Color.Yellow;
            this.lblDividend1.Font = new System.Drawing.Font("Microsoft Sans Serif", 3F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDividend1.Location = new System.Drawing.Point(41, 49);
            this.lblDividend1.Margin = new System.Windows.Forms.Padding(0);
            this.lblDividend1.Name = "lblDividend1";
            this.lblDividend1.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.lblDividend1.Size = new System.Drawing.Size(18, 8);
            this.lblDividend1.TabIndex = 7;
            this.lblDividend1.Text = "50";
            this.lblDividend1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 3F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(23, 57);
            this.label4.Margin = new System.Windows.Forms.Padding(0);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.label4.Size = new System.Drawing.Size(18, 8);
            this.label4.TabIndex = 8;
            this.label4.Text = "Arður";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 3F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(5, 57);
            this.label5.Margin = new System.Windows.Forms.Padding(0);
            this.label5.Name = "label5";
            this.label5.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.label5.Size = new System.Drawing.Size(18, 8);
            this.label5.TabIndex = 9;
            this.label5.Text = "50%";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDividend2
            // 
            this.lblDividend2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lblDividend2.Font = new System.Drawing.Font("Microsoft Sans Serif", 3F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDividend2.Location = new System.Drawing.Point(41, 57);
            this.lblDividend2.Margin = new System.Windows.Forms.Padding(0);
            this.lblDividend2.Name = "lblDividend2";
            this.lblDividend2.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.lblDividend2.Size = new System.Drawing.Size(18, 8);
            this.lblDividend2.TabIndex = 10;
            this.lblDividend2.Text = "200";
            this.lblDividend2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 3F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(5, 65);
            this.label7.Margin = new System.Windows.Forms.Padding(0);
            this.label7.Name = "label7";
            this.label7.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.label7.Size = new System.Drawing.Size(18, 8);
            this.label7.TabIndex = 11;
            this.label7.Text = "75%";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 3F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(23, 65);
            this.label8.Margin = new System.Windows.Forms.Padding(0);
            this.label8.Name = "label8";
            this.label8.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.label8.Size = new System.Drawing.Size(18, 8);
            this.label8.TabIndex = 12;
            this.label8.Text = "Arður";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDividend3
            // 
            this.lblDividend3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblDividend3.Font = new System.Drawing.Font("Microsoft Sans Serif", 3F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDividend3.Location = new System.Drawing.Point(41, 65);
            this.lblDividend3.Margin = new System.Windows.Forms.Padding(0);
            this.lblDividend3.Name = "lblDividend3";
            this.lblDividend3.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.lblDividend3.Size = new System.Drawing.Size(18, 8);
            this.lblDividend3.TabIndex = 13;
            this.lblDividend3.Text = "500";
            this.lblDividend3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDividend4
            // 
            this.lblDividend4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lblDividend4.Font = new System.Drawing.Font("Microsoft Sans Serif", 3F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDividend4.Location = new System.Drawing.Point(41, 73);
            this.lblDividend4.Margin = new System.Windows.Forms.Padding(0);
            this.lblDividend4.Name = "lblDividend4";
            this.lblDividend4.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.lblDividend4.Size = new System.Drawing.Size(18, 8);
            this.lblDividend4.TabIndex = 16;
            this.lblDividend4.Text = "1000";
            this.lblDividend4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 3F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(23, 73);
            this.label11.Margin = new System.Windows.Forms.Padding(0);
            this.label11.Name = "label11";
            this.label11.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.label11.Size = new System.Drawing.Size(18, 8);
            this.label11.TabIndex = 15;
            this.label11.Text = "Arður";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 3F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(5, 73);
            this.label12.Margin = new System.Windows.Forms.Padding(0);
            this.label12.Name = "label12";
            this.label12.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.label12.Size = new System.Drawing.Size(18, 8);
            this.label12.TabIndex = 14;
            this.label12.Text = "100%";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // CompanyCardTinyControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlCard);
            this.Name = "CompanyCardTinyControl";
            this.Size = new System.Drawing.Size(64, 87);
            this.Load += new System.EventHandler(this.CompanyCardTinyControl_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.CompanyCardTinyControl_Paint);
            this.pnlCard.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picImage;
        private System.Windows.Forms.Label lblEmployeeCount;
        private System.Windows.Forms.Label lblCost;
        private System.Windows.Forms.Panel pnlCard;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblDividend4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblDividend3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblDividend2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblDividend1;
        private System.Windows.Forms.Label label1;
    }
}
