﻿using Game.Controls;
using Game.Entities;
using Game.Entities.Cards;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Game.Dialogs
{
    public partial class CompanyCardDialog : Form
    {
        public CompanyCardControl cardControl;

        public CompanyCardDialog(Company company)
        {
            InitializeComponent();

            //this.Location = new Point(x, y);
            cardControl = new CompanyCardControl(company);
            cardControl.ControlClicked += CardControl_ControlClicked;
            Controls.Add(cardControl);
            cardControl.Refresh();
        }

        private void CardControl_ControlClicked(object sender, EventArgs e)
        {
            this.Close();
        }

        public static DialogResult ShowDialog(Company company)
        {
            // using construct ensures the resources are freed when form is closed
            using (var form = new CompanyCardDialog(company))
            {
                return form.ShowDialog();
            }
        }
    }
}
