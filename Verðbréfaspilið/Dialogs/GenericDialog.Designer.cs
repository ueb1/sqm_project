﻿namespace Game.Dialogs
{
    partial class GenericDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblText = new System.Windows.Forms.Label();
            this.btnLeft = new System.Windows.Forms.Button();
            this.btnMiddle = new System.Windows.Forms.Button();
            this.btnRight = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblText
            // 
            this.lblText.AutoSize = true;
            this.lblText.BackColor = System.Drawing.Color.Transparent;
            this.lblText.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblText.Location = new System.Drawing.Point(0, 0);
            this.lblText.MaximumSize = new System.Drawing.Size(384, 500);
            this.lblText.MinimumSize = new System.Drawing.Size(384, 183);
            this.lblText.Name = "lblText";
            this.lblText.Padding = new System.Windows.Forms.Padding(10, 10, 10, 0);
            this.lblText.Size = new System.Drawing.Size(384, 183);
            this.lblText.TabIndex = 3;
            this.lblText.Text = "Question";
            this.lblText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnLeft
            // 
            this.btnLeft.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.btnLeft.Location = new System.Drawing.Point(16, 19);
            this.btnLeft.Name = "btnLeft";
            this.btnLeft.Size = new System.Drawing.Size(100, 40);
            this.btnLeft.TabIndex = 5;
            this.btnLeft.Text = "Já";
            this.btnLeft.UseVisualStyleBackColor = true;
            this.btnLeft.Visible = false;
            // 
            // btnMiddle
            // 
            this.btnMiddle.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.btnMiddle.Location = new System.Drawing.Point(142, 19);
            this.btnMiddle.Name = "btnMiddle";
            this.btnMiddle.Size = new System.Drawing.Size(100, 40);
            this.btnMiddle.TabIndex = 7;
            this.btnMiddle.Text = "Í lagi";
            this.btnMiddle.UseVisualStyleBackColor = true;
            this.btnMiddle.Visible = false;
            // 
            // btnRight
            // 
            this.btnRight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRight.DialogResult = System.Windows.Forms.DialogResult.No;
            this.btnRight.Location = new System.Drawing.Point(268, 19);
            this.btnRight.Name = "btnRight";
            this.btnRight.Size = new System.Drawing.Size(100, 40);
            this.btnRight.TabIndex = 6;
            this.btnRight.Text = "Nei";
            this.btnRight.UseVisualStyleBackColor = true;
            this.btnRight.Visible = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.btnRight);
            this.panel1.Controls.Add(this.btnLeft);
            this.panel1.Controls.Add(this.btnMiddle);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 183);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(384, 75);
            this.panel1.TabIndex = 8;
            // 
            // GenericDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(384, 260);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblText);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximumSize = new System.Drawing.Size(384, 600);
            this.MinimumSize = new System.Drawing.Size(384, 260);
            this.Name = "GenericDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "GenericDialog";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.GenericDialog_Paint);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblText;
        private System.Windows.Forms.Button btnLeft;
        private System.Windows.Forms.Button btnMiddle;
        private System.Windows.Forms.Button btnRight;
        private System.Windows.Forms.Panel panel1;
    }
}