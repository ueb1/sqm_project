﻿namespace Game.Dialogs
{
    partial class CompanyCellDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblText = new System.Windows.Forms.Label();
            this.btnFinish = new System.Windows.Forms.Button();
            this.btnWait = new System.Windows.Forms.Button();
            this.pnlBuy = new System.Windows.Forms.Panel();
            this.lblTotal = new System.Windows.Forms.Label();
            this.lblRate = new System.Windows.Forms.Label();
            this.lblCurrency = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblTotalDesc = new System.Windows.Forms.Label();
            this.radBuy75 = new System.Windows.Forms.RadioButton();
            this.radBuy50 = new System.Windows.Forms.RadioButton();
            this.radBuy25 = new System.Windows.Forms.RadioButton();
            this.radBuy0 = new System.Windows.Forms.RadioButton();
            this.companyCardControl = new Game.Controls.CompanyCardControl();
            this.pnlBuy.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblText
            // 
            this.lblText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblText.Location = new System.Drawing.Point(226, 9);
            this.lblText.Name = "lblText";
            this.lblText.Size = new System.Drawing.Size(246, 44);
            this.lblText.TabIndex = 1;
            this.lblText.Text = "Viltu kaupa þetta fyrirtæki?";
            this.lblText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnFinish
            // 
            this.btnFinish.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnFinish.Location = new System.Drawing.Point(382, 270);
            this.btnFinish.Name = "btnFinish";
            this.btnFinish.Size = new System.Drawing.Size(90, 30);
            this.btnFinish.TabIndex = 5;
            this.btnFinish.Text = "Áfram";
            this.btnFinish.UseVisualStyleBackColor = true;
            this.btnFinish.Click += new System.EventHandler(this.btnFinish_Click);
            // 
            // btnWait
            // 
            this.btnWait.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnWait.Location = new System.Drawing.Point(226, 270);
            this.btnWait.Name = "btnWait";
            this.btnWait.Size = new System.Drawing.Size(90, 30);
            this.btnWait.TabIndex = 6;
            this.btnWait.Text = "Bíða";
            this.btnWait.UseVisualStyleBackColor = true;
            this.btnWait.Click += new System.EventHandler(this.btnWait_Click);
            // 
            // pnlBuy
            // 
            this.pnlBuy.Controls.Add(this.lblTotal);
            this.pnlBuy.Controls.Add(this.lblRate);
            this.pnlBuy.Controls.Add(this.lblCurrency);
            this.pnlBuy.Controls.Add(this.label1);
            this.pnlBuy.Controls.Add(this.lblTotalDesc);
            this.pnlBuy.Controls.Add(this.radBuy75);
            this.pnlBuy.Controls.Add(this.radBuy50);
            this.pnlBuy.Controls.Add(this.radBuy25);
            this.pnlBuy.Controls.Add(this.radBuy0);
            this.pnlBuy.Location = new System.Drawing.Point(226, 56);
            this.pnlBuy.Name = "pnlBuy";
            this.pnlBuy.Size = new System.Drawing.Size(246, 208);
            this.pnlBuy.TabIndex = 16;
            // 
            // lblTotal
            // 
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(119, 168);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(65, 17);
            this.lblTotal.TabIndex = 24;
            this.lblTotal.Text = "36000";
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblRate
            // 
            this.lblRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRate.Location = new System.Drawing.Point(0, 0);
            this.lblRate.Name = "lblRate";
            this.lblRate.Size = new System.Drawing.Size(246, 27);
            this.lblRate.TabIndex = 23;
            this.lblRate.Text = "Gengið er: 1";
            this.lblRate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCurrency
            // 
            this.lblCurrency.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrency.Location = new System.Drawing.Point(176, 168);
            this.lblCurrency.Name = "lblCurrency";
            this.lblCurrency.Size = new System.Drawing.Size(31, 17);
            this.lblCurrency.TabIndex = 22;
            this.lblCurrency.Text = "kr.";
            this.lblCurrency.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(38, 166);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(170, 2);
            this.label1.TabIndex = 21;
            // 
            // lblTotalDesc
            // 
            this.lblTotalDesc.AutoSize = true;
            this.lblTotalDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalDesc.Location = new System.Drawing.Point(43, 168);
            this.lblTotalDesc.Name = "lblTotalDesc";
            this.lblTotalDesc.Size = new System.Drawing.Size(70, 17);
            this.lblTotalDesc.TabIndex = 20;
            this.lblTotalDesc.Text = "Samtals:";
            // 
            // radBuy75
            // 
            this.radBuy75.AutoSize = true;
            this.radBuy75.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radBuy75.Location = new System.Drawing.Point(55, 138);
            this.radBuy75.Name = "radBuy75";
            this.radBuy75.Size = new System.Drawing.Size(140, 21);
            this.radBuy75.TabIndex = 19;
            this.radBuy75.Text = "Kaupa 75% hlut";
            this.radBuy75.UseVisualStyleBackColor = true;
            this.radBuy75.Click += new System.EventHandler(this.radioButton_clicked);
            // 
            // radBuy50
            // 
            this.radBuy50.AutoSize = true;
            this.radBuy50.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radBuy50.Location = new System.Drawing.Point(55, 107);
            this.radBuy50.Name = "radBuy50";
            this.radBuy50.Size = new System.Drawing.Size(140, 21);
            this.radBuy50.TabIndex = 18;
            this.radBuy50.Text = "Kaupa 50% hlut";
            this.radBuy50.UseVisualStyleBackColor = true;
            this.radBuy50.Click += new System.EventHandler(this.radioButton_clicked);
            // 
            // radBuy25
            // 
            this.radBuy25.AutoSize = true;
            this.radBuy25.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.radBuy25.Location = new System.Drawing.Point(55, 76);
            this.radBuy25.Name = "radBuy25";
            this.radBuy25.Size = new System.Drawing.Size(140, 21);
            this.radBuy25.TabIndex = 17;
            this.radBuy25.Text = "Kaupa 25% hlut";
            this.radBuy25.UseVisualStyleBackColor = true;
            this.radBuy25.Click += new System.EventHandler(this.radioButton_clicked);
            // 
            // radBuy0
            // 
            this.radBuy0.AutoSize = true;
            this.radBuy0.Checked = true;
            this.radBuy0.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radBuy0.Location = new System.Drawing.Point(55, 45);
            this.radBuy0.Name = "radBuy0";
            this.radBuy0.Size = new System.Drawing.Size(85, 21);
            this.radBuy0.TabIndex = 16;
            this.radBuy0.TabStop = true;
            this.radBuy0.Text = "Nei takk";
            this.radBuy0.UseVisualStyleBackColor = true;
            this.radBuy0.Click += new System.EventHandler(this.radioButton_clicked);
            // 
            // companyCardControl
            // 
            this.companyCardControl.Location = new System.Drawing.Point(0, 0);
            this.companyCardControl.Name = "companyCardControl";
            this.companyCardControl.Size = new System.Drawing.Size(220, 300);
            this.companyCardControl.TabIndex = 0;
            // 
            // CompanyCellDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 302);
            this.Controls.Add(this.pnlBuy);
            this.Controls.Add(this.btnWait);
            this.Controls.Add(this.btnFinish);
            this.Controls.Add(this.lblText);
            this.Controls.Add(this.companyCardControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "CompanyCellDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "CompanyCellDialog";
            this.pnlBuy.ResumeLayout(false);
            this.pnlBuy.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblText;
        private System.Windows.Forms.Button btnFinish;
        private Controls.CompanyCardControl companyCardControl;
        private System.Windows.Forms.Button btnWait;
        private System.Windows.Forms.Panel pnlBuy;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label lblRate;
        private System.Windows.Forms.Label lblCurrency;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblTotalDesc;
        private System.Windows.Forms.RadioButton radBuy75;
        private System.Windows.Forms.RadioButton radBuy50;
        private System.Windows.Forms.RadioButton radBuy25;
        private System.Windows.Forms.RadioButton radBuy0;
    }
}