﻿using Game.Controls;
using Game.Entities;
using Game.Entities.Cards;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Game.Dialogs
{
    public partial class CompanyCellDialog : Form
    {
        private int costEachShare;
        private bool notEnoughCash;
        private Wallet playersWallet;

        public int SharesBought { get; private set; }

        public CompanyCellDialog(Company company, int rate, int sharesAvailable, Wallet playersWallet)
        {
            InitializeComponent();

            this.playersWallet = playersWallet;
            SharesBought = 0;
            companyCardControl.init(company);
            if (sharesAvailable > 0)
            {
                costEachShare = company.CostEachShare * rate;
                lblRate.Text = "Gengið er: " + rate;
                lblTotal.Text = "0";

                if (rate < 3 || sharesAvailable < 3)
                    radBuy75.Enabled = false;

                if (rate < 2 || sharesAvailable < 2)
                    radBuy50.Enabled = false;
            }
            else
            {
                pnlBuy.Visible = false;
                lblText.Text = "Engin hlutabréf til sölu!";
                lblText.ForeColor = Color.DarkRed;
                btnWait.Visible = false;
            }
        }

        public static DialogResult ShowDialog(string text, MessageBoxButtons buttons)
        {
            // using construct ensures the resources are freed when form is closed
            using (var form = new GenericDialog(text))
            {
                return form.ShowDialog();
            }
        }

        private void btnWait_Click(object sender, EventArgs e)
        {

        }

        private void radioButton_clicked(object sender, EventArgs e)
        {
            int total = 0;
            if(sender == radBuy25)
            {
                total = costEachShare;
            }
            else if (sender == radBuy50)
            {
                total = costEachShare * 2;
            }
            else if (sender == radBuy75)
            {
                total = costEachShare * 3;
            }

            lblTotal.Text = total.ToString("N0");
            if (total > playersWallet.Cash)
            {
                notEnoughCash = true;
                lblTotal.ForeColor = Color.DarkRed;
                lblCurrency.ForeColor = Color.DarkRed;
            }
        }

        private void btnFinish_Click(object sender, EventArgs e)
        {
            if (radBuy0.Checked)
                SharesBought = 0;
            else if (radBuy25.Checked)
                SharesBought = 1;
            else if (radBuy50.Checked)
                SharesBought = 2;
            else if (radBuy75.Checked)
                SharesBought = 3;
        }
    }
}
