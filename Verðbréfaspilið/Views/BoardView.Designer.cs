﻿namespace Game.Views
{
    partial class BoardView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.rollTimer = new System.Windows.Forms.Timer(this.components);
            this.moveTimer = new System.Windows.Forms.Timer(this.components);
            this.pnlBoard = new System.Windows.Forms.Panel();
            this.flowCompanyCards = new System.Windows.Forms.FlowLayoutPanel();
            this.dbgActivateFateCard = new System.Windows.Forms.Button();
            this.dbgFateCardId = new System.Windows.Forms.TextBox();
            this.numMovement = new System.Windows.Forms.NumericUpDown();
            this.btnCellEvent = new System.Windows.Forms.Button();
            this.btnFinishTurn = new System.Windows.Forms.Button();
            this.picDie1 = new System.Windows.Forms.PictureBox();
            this.picDie2 = new System.Windows.Forms.PictureBox();
            this.pnlWallet = new System.Windows.Forms.Panel();
            this.tabCards = new FlatTabControl.FlatTabControl();
            this.tabCompanyCards = new System.Windows.Forms.TabPage();
            this.tabPrivateCompanyCards = new System.Windows.Forms.TabPage();
            this.pnl = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.lblBondAmount = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblFundLetterAmount = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblSharesAmount = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblPlayerName = new System.Windows.Forms.Label();
            this.lblCash = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabBondCards = new System.Windows.Forms.TabPage();
            this.tabBankCards = new System.Windows.Forms.TabPage();
            this.tabCarCards = new System.Windows.Forms.TabPage();
            this.flowPrivateCompanyCards = new System.Windows.Forms.FlowLayoutPanel();
            this.flowBondCards = new System.Windows.Forms.FlowLayoutPanel();
            this.flowBankCards = new System.Windows.Forms.FlowLayoutPanel();
            this.flowCarCards = new System.Windows.Forms.FlowLayoutPanel();
            this.pnlStockMarket = new System.Windows.Forms.Panel();
            this.flowCompanies1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowCompanies4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowCompanies2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowCompanies3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowCompanies5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowCompanies6 = new System.Windows.Forms.FlowLayoutPanel();
            this.exchangeRateCard = new Game.Controls.ExchangeRateTableControl();
            this.pnlBoard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMovement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDie1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDie2)).BeginInit();
            this.pnlWallet.SuspendLayout();
            this.tabCards.SuspendLayout();
            this.tabCompanyCards.SuspendLayout();
            this.tabPrivateCompanyCards.SuspendLayout();
            this.pnl.SuspendLayout();
            this.tabBondCards.SuspendLayout();
            this.tabBankCards.SuspendLayout();
            this.tabCarCards.SuspendLayout();
            this.pnlStockMarket.SuspendLayout();
            this.SuspendLayout();
            // 
            // rollTimer
            // 
            this.rollTimer.Tick += new System.EventHandler(this.rollTimer_Tick);
            // 
            // moveTimer
            // 
            this.moveTimer.Interval = 200;
            this.moveTimer.Tick += new System.EventHandler(this.moveTimer_Tick);
            // 
            // pnlBoard
            // 
            this.pnlBoard.Controls.Add(this.dbgActivateFateCard);
            this.pnlBoard.Controls.Add(this.dbgFateCardId);
            this.pnlBoard.Controls.Add(this.numMovement);
            this.pnlBoard.Controls.Add(this.btnCellEvent);
            this.pnlBoard.Controls.Add(this.btnFinishTurn);
            this.pnlBoard.Controls.Add(this.exchangeRateCard);
            this.pnlBoard.Controls.Add(this.picDie1);
            this.pnlBoard.Controls.Add(this.picDie2);
            this.pnlBoard.Location = new System.Drawing.Point(0, 0);
            this.pnlBoard.Margin = new System.Windows.Forms.Padding(0);
            this.pnlBoard.Name = "pnlBoard";
            this.pnlBoard.Size = new System.Drawing.Size(1280, 585);
            this.pnlBoard.TabIndex = 5;
            this.pnlBoard.Click += new System.EventHandler(this.picBoard_Click);
            // 
            // flowCompanyCards
            // 
            this.flowCompanyCards.AutoSize = true;
            this.flowCompanyCards.Location = new System.Drawing.Point(0, 0);
            this.flowCompanyCards.MaximumSize = new System.Drawing.Size(1000, 600);
            this.flowCompanyCards.Name = "flowCompanyCards";
            this.flowCompanyCards.Size = new System.Drawing.Size(1000, 211);
            this.flowCompanyCards.TabIndex = 0;
            // 
            // dbgActivateFateCard
            // 
            this.dbgActivateFateCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dbgActivateFateCard.Location = new System.Drawing.Point(494, 190);
            this.dbgActivateFateCard.Name = "dbgActivateFateCard";
            this.dbgActivateFateCard.Size = new System.Drawing.Size(91, 23);
            this.dbgActivateFateCard.TabIndex = 9;
            this.dbgActivateFateCard.Text = "Gluggaspjald";
            this.dbgActivateFateCard.UseVisualStyleBackColor = true;
            this.dbgActivateFateCard.Visible = false;
            this.dbgActivateFateCard.Click += new System.EventHandler(this.dbgActivateFateCard_Click);
            // 
            // dbgFateCardId
            // 
            this.dbgFateCardId.Location = new System.Drawing.Point(591, 192);
            this.dbgFateCardId.Name = "dbgFateCardId";
            this.dbgFateCardId.Size = new System.Drawing.Size(36, 20);
            this.dbgFateCardId.TabIndex = 8;
            this.dbgFateCardId.Text = "1";
            this.dbgFateCardId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.dbgFateCardId.Visible = false;
            // 
            // numMovement
            // 
            this.numMovement.Location = new System.Drawing.Point(574, 303);
            this.numMovement.Name = "numMovement";
            this.numMovement.Size = new System.Drawing.Size(120, 20);
            this.numMovement.TabIndex = 7;
            this.numMovement.Visible = false;
            // 
            // btnCellEvent
            // 
            this.btnCellEvent.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCellEvent.Location = new System.Drawing.Point(355, 329);
            this.btnCellEvent.Name = "btnCellEvent";
            this.btnCellEvent.Size = new System.Drawing.Size(114, 67);
            this.btnCellEvent.TabIndex = 7;
            this.btnCellEvent.Text = "Opna reit";
            this.btnCellEvent.UseVisualStyleBackColor = true;
            this.btnCellEvent.Visible = false;
            // 
            // btnFinishTurn
            // 
            this.btnFinishTurn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFinishTurn.Location = new System.Drawing.Point(574, 329);
            this.btnFinishTurn.Name = "btnFinishTurn";
            this.btnFinishTurn.Size = new System.Drawing.Size(114, 67);
            this.btnFinishTurn.TabIndex = 6;
            this.btnFinishTurn.Text = "Næsti spilari";
            this.btnFinishTurn.UseVisualStyleBackColor = true;
            this.btnFinishTurn.Visible = false;
            // 
            // picDie1
            // 
            this.picDie1.BackColor = System.Drawing.Color.Transparent;
            this.picDie1.Image = global::Game.Properties.Resources.Die1;
            this.picDie1.Location = new System.Drawing.Point(591, 287);
            this.picDie1.Name = "picDie1";
            this.picDie1.Size = new System.Drawing.Size(36, 36);
            this.picDie1.TabIndex = 3;
            this.picDie1.TabStop = false;
            // 
            // picDie2
            // 
            this.picDie2.BackColor = System.Drawing.Color.Transparent;
            this.picDie2.Image = global::Game.Properties.Resources.Die1;
            this.picDie2.Location = new System.Drawing.Point(633, 287);
            this.picDie2.Name = "picDie2";
            this.picDie2.Size = new System.Drawing.Size(36, 36);
            this.picDie2.TabIndex = 4;
            this.picDie2.TabStop = false;
            // 
            // pnlWallet
            // 
            this.pnlWallet.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pnlWallet.Controls.Add(this.tabCards);
            this.pnlWallet.Controls.Add(this.pnl);
            this.pnlWallet.Location = new System.Drawing.Point(0, 585);
            this.pnlWallet.Margin = new System.Windows.Forms.Padding(0);
            this.pnlWallet.Name = "pnlWallet";
            this.pnlWallet.Size = new System.Drawing.Size(1280, 245);
            this.pnlWallet.TabIndex = 6;
            // 
            // tabCards
            // 
            this.tabCards.Controls.Add(this.tabCompanyCards);
            this.tabCards.Controls.Add(this.tabPrivateCompanyCards);
            this.tabCards.Controls.Add(this.tabBondCards);
            this.tabCards.Controls.Add(this.tabBankCards);
            this.tabCards.Controls.Add(this.tabCarCards);
            this.tabCards.Location = new System.Drawing.Point(227, 3);
            this.tabCards.myBackColor = System.Drawing.SystemColors.ControlDark;
            this.tabCards.Name = "tabCards";
            this.tabCards.SelectedIndex = 0;
            this.tabCards.Size = new System.Drawing.Size(1028, 239);
            this.tabCards.TabIndex = 2;
            // 
            // tabCompanyCards
            // 
            this.tabCompanyCards.AutoScroll = true;
            this.tabCompanyCards.BackColor = System.Drawing.Color.Lavender;
            this.tabCompanyCards.Controls.Add(this.flowCompanyCards);
            this.tabCompanyCards.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tabCompanyCards.Location = new System.Drawing.Point(4, 25);
            this.tabCompanyCards.Name = "tabCompanyCards";
            this.tabCompanyCards.Padding = new System.Windows.Forms.Padding(3);
            this.tabCompanyCards.Size = new System.Drawing.Size(1020, 210);
            this.tabCompanyCards.TabIndex = 0;
            this.tabCompanyCards.Text = "Hlutabréf";
            // 
            // tabPrivateCompanyCards
            // 
            this.tabPrivateCompanyCards.AutoScroll = true;
            this.tabPrivateCompanyCards.BackColor = System.Drawing.Color.Honeydew;
            this.tabPrivateCompanyCards.Controls.Add(this.flowPrivateCompanyCards);
            this.tabPrivateCompanyCards.Location = new System.Drawing.Point(4, 25);
            this.tabPrivateCompanyCards.Name = "tabPrivateCompanyCards";
            this.tabPrivateCompanyCards.Padding = new System.Windows.Forms.Padding(3);
            this.tabPrivateCompanyCards.Size = new System.Drawing.Size(1020, 210);
            this.tabPrivateCompanyCards.TabIndex = 1;
            this.tabPrivateCompanyCards.Text = "Einkafyrirtæki";
            // 
            // pnl
            // 
            this.pnl.Controls.Add(this.button1);
            this.pnl.Controls.Add(this.lblBondAmount);
            this.pnl.Controls.Add(this.label8);
            this.pnl.Controls.Add(this.lblFundLetterAmount);
            this.pnl.Controls.Add(this.label6);
            this.pnl.Controls.Add(this.lblSharesAmount);
            this.pnl.Controls.Add(this.label4);
            this.pnl.Controls.Add(this.lblPlayerName);
            this.pnl.Controls.Add(this.lblCash);
            this.pnl.Controls.Add(this.label1);
            this.pnl.Location = new System.Drawing.Point(3, 3);
            this.pnl.Name = "pnl";
            this.pnl.Size = new System.Drawing.Size(218, 239);
            this.pnl.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(69, 213);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "Nánar";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // lblBondAmount
            // 
            this.lblBondAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBondAmount.Location = new System.Drawing.Point(115, 122);
            this.lblBondAmount.Name = "lblBondAmount";
            this.lblBondAmount.Size = new System.Drawing.Size(100, 23);
            this.lblBondAmount.TabIndex = 8;
            this.lblBondAmount.Text = "200.000";
            this.lblBondAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(4, 122);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(105, 23);
            this.label8.TabIndex = 7;
            this.label8.Text = "Spariskírteini:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblFundLetterAmount
            // 
            this.lblFundLetterAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFundLetterAmount.Location = new System.Drawing.Point(115, 99);
            this.lblFundLetterAmount.Name = "lblFundLetterAmount";
            this.lblFundLetterAmount.Size = new System.Drawing.Size(100, 23);
            this.lblFundLetterAmount.TabIndex = 6;
            this.lblFundLetterAmount.Text = "200.000";
            this.lblFundLetterAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(4, 99);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 23);
            this.label6.TabIndex = 5;
            this.label6.Text = "Sjóðsbréf:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblSharesAmount
            // 
            this.lblSharesAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSharesAmount.Location = new System.Drawing.Point(115, 76);
            this.lblSharesAmount.Name = "lblSharesAmount";
            this.lblSharesAmount.Size = new System.Drawing.Size(100, 23);
            this.lblSharesAmount.TabIndex = 4;
            this.lblSharesAmount.Text = "200.000";
            this.lblSharesAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(4, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 23);
            this.label4.TabIndex = 3;
            this.label4.Text = "Hlutabréf:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblPlayerName
            // 
            this.lblPlayerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayerName.Location = new System.Drawing.Point(3, 0);
            this.lblPlayerName.Name = "lblPlayerName";
            this.lblPlayerName.Size = new System.Drawing.Size(212, 23);
            this.lblPlayerName.TabIndex = 2;
            this.lblPlayerName.Text = "Player name";
            this.lblPlayerName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCash
            // 
            this.lblCash.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCash.Location = new System.Drawing.Point(115, 53);
            this.lblCash.Name = "lblCash";
            this.lblCash.Size = new System.Drawing.Size(100, 23);
            this.lblCash.TabIndex = 1;
            this.lblCash.Text = "200.000";
            this.lblCash.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(4, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Reiðufé:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabBondCards
            // 
            this.tabBondCards.AutoScroll = true;
            this.tabBondCards.BackColor = System.Drawing.Color.MistyRose;
            this.tabBondCards.Controls.Add(this.flowBondCards);
            this.tabBondCards.Location = new System.Drawing.Point(4, 25);
            this.tabBondCards.Name = "tabBondCards";
            this.tabBondCards.Size = new System.Drawing.Size(1020, 210);
            this.tabBondCards.TabIndex = 2;
            this.tabBondCards.Text = "Spariskírteini";
            // 
            // tabBankCards
            // 
            this.tabBankCards.BackColor = System.Drawing.Color.Linen;
            this.tabBankCards.Controls.Add(this.flowBankCards);
            this.tabBankCards.Location = new System.Drawing.Point(4, 25);
            this.tabBankCards.Name = "tabBankCards";
            this.tabBankCards.Size = new System.Drawing.Size(1020, 210);
            this.tabBankCards.TabIndex = 3;
            this.tabBankCards.Text = "Bankabækur";
            // 
            // tabCarCards
            // 
            this.tabCarCards.BackColor = System.Drawing.Color.LavenderBlush;
            this.tabCarCards.Controls.Add(this.flowCarCards);
            this.tabCarCards.Location = new System.Drawing.Point(4, 25);
            this.tabCarCards.Name = "tabCarCards";
            this.tabCarCards.Size = new System.Drawing.Size(1020, 210);
            this.tabCarCards.TabIndex = 4;
            this.tabCarCards.Text = "Bílar";
            // 
            // flowPrivateCompanyCards
            // 
            this.flowPrivateCompanyCards.AutoSize = true;
            this.flowPrivateCompanyCards.Location = new System.Drawing.Point(0, 0);
            this.flowPrivateCompanyCards.MaximumSize = new System.Drawing.Size(1000, 600);
            this.flowPrivateCompanyCards.Name = "flowPrivateCompanyCards";
            this.flowPrivateCompanyCards.Size = new System.Drawing.Size(1000, 211);
            this.flowPrivateCompanyCards.TabIndex = 1;
            // 
            // flowBondCards
            // 
            this.flowBondCards.AutoSize = true;
            this.flowBondCards.Location = new System.Drawing.Point(0, 0);
            this.flowBondCards.MaximumSize = new System.Drawing.Size(1000, 600);
            this.flowBondCards.Name = "flowBondCards";
            this.flowBondCards.Size = new System.Drawing.Size(1000, 211);
            this.flowBondCards.TabIndex = 1;
            // 
            // flowBankCards
            // 
            this.flowBankCards.AutoSize = true;
            this.flowBankCards.Location = new System.Drawing.Point(0, 0);
            this.flowBankCards.MaximumSize = new System.Drawing.Size(1000, 600);
            this.flowBankCards.Name = "flowBankCards";
            this.flowBankCards.Size = new System.Drawing.Size(1000, 211);
            this.flowBankCards.TabIndex = 1;
            // 
            // flowCarCards
            // 
            this.flowCarCards.AutoSize = true;
            this.flowCarCards.Location = new System.Drawing.Point(0, 0);
            this.flowCarCards.MaximumSize = new System.Drawing.Size(1000, 600);
            this.flowCarCards.Name = "flowCarCards";
            this.flowCarCards.Size = new System.Drawing.Size(996, 211);
            this.flowCarCards.TabIndex = 1;
            // 
            // pnlStockMarket
            // 
            this.pnlStockMarket.BackColor = System.Drawing.Color.LemonChiffon;
            this.pnlStockMarket.Controls.Add(this.flowCompanies6);
            this.pnlStockMarket.Controls.Add(this.flowCompanies5);
            this.pnlStockMarket.Controls.Add(this.flowCompanies4);
            this.pnlStockMarket.Controls.Add(this.flowCompanies3);
            this.pnlStockMarket.Controls.Add(this.flowCompanies2);
            this.pnlStockMarket.Controls.Add(this.flowCompanies1);
            this.pnlStockMarket.Location = new System.Drawing.Point(0, 0);
            this.pnlStockMarket.Name = "pnlStockMarket";
            this.pnlStockMarket.Size = new System.Drawing.Size(1280, 585);
            this.pnlStockMarket.TabIndex = 7;
            // 
            // flowCompanies1
            // 
            this.flowCompanies1.Location = new System.Drawing.Point(3, 3);
            this.flowCompanies1.Name = "flowCompanies1";
            this.flowCompanies1.Size = new System.Drawing.Size(565, 87);
            this.flowCompanies1.TabIndex = 0;
            // 
            // flowCompanies4
            // 
            this.flowCompanies4.Location = new System.Drawing.Point(3, 282);
            this.flowCompanies4.Name = "flowCompanies4";
            this.flowCompanies4.Size = new System.Drawing.Size(565, 87);
            this.flowCompanies4.TabIndex = 1;
            // 
            // flowCompanies2
            // 
            this.flowCompanies2.Location = new System.Drawing.Point(3, 96);
            this.flowCompanies2.Name = "flowCompanies2";
            this.flowCompanies2.Size = new System.Drawing.Size(565, 87);
            this.flowCompanies2.TabIndex = 1;
            // 
            // flowCompanies3
            // 
            this.flowCompanies3.Location = new System.Drawing.Point(3, 189);
            this.flowCompanies3.Name = "flowCompanies3";
            this.flowCompanies3.Size = new System.Drawing.Size(565, 87);
            this.flowCompanies3.TabIndex = 1;
            // 
            // flowCompanies5
            // 
            this.flowCompanies5.Location = new System.Drawing.Point(3, 375);
            this.flowCompanies5.Name = "flowCompanies5";
            this.flowCompanies5.Size = new System.Drawing.Size(565, 87);
            this.flowCompanies5.TabIndex = 1;
            // 
            // flowCompanies6
            // 
            this.flowCompanies6.Location = new System.Drawing.Point(3, 468);
            this.flowCompanies6.Name = "flowCompanies6";
            this.flowCompanies6.Size = new System.Drawing.Size(565, 87);
            this.flowCompanies6.TabIndex = 2;
            // 
            // exchangeRateCard
            // 
            this.exchangeRateCard.BackColor = System.Drawing.Color.White;
            this.exchangeRateCard.Location = new System.Drawing.Point(745, 200);
            this.exchangeRateCard.Name = "exchangeRateCard";
            this.exchangeRateCard.Size = new System.Drawing.Size(280, 200);
            this.exchangeRateCard.TabIndex = 5;
            // 
            // BoardView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.Controls.Add(this.pnlStockMarket);
            this.Controls.Add(this.pnlWallet);
            this.Controls.Add(this.pnlBoard);
            this.DoubleBuffered = true;
            this.Name = "BoardView";
            this.Size = new System.Drawing.Size(1280, 830);
            this.pnlBoard.ResumeLayout(false);
            this.pnlBoard.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMovement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDie1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDie2)).EndInit();
            this.pnlWallet.ResumeLayout(false);
            this.tabCards.ResumeLayout(false);
            this.tabCompanyCards.ResumeLayout(false);
            this.tabCompanyCards.PerformLayout();
            this.tabPrivateCompanyCards.ResumeLayout(false);
            this.tabPrivateCompanyCards.PerformLayout();
            this.pnl.ResumeLayout(false);
            this.tabBondCards.ResumeLayout(false);
            this.tabBondCards.PerformLayout();
            this.tabBankCards.ResumeLayout(false);
            this.tabBankCards.PerformLayout();
            this.tabCarCards.ResumeLayout(false);
            this.tabCarCards.PerformLayout();
            this.pnlStockMarket.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox picDie2;
        private System.Windows.Forms.PictureBox picDie1;
        private System.Windows.Forms.Timer rollTimer;
        private System.Windows.Forms.Timer moveTimer;
        private System.Windows.Forms.Panel pnlBoard;
        private System.Windows.Forms.Panel pnlWallet;
        private System.Windows.Forms.FlowLayoutPanel flowCompanyCards;
        private System.Windows.Forms.Panel pnl;
        private Controls.ExchangeRateTableControl exchangeRateCard;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblBondAmount;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblFundLetterAmount;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblSharesAmount;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblPlayerName;
        private System.Windows.Forms.Label lblCash;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button btnFinishTurn;
        public System.Windows.Forms.Button btnCellEvent;
        private System.Windows.Forms.NumericUpDown numMovement;
        private System.Windows.Forms.Button dbgActivateFateCard;
        private System.Windows.Forms.TextBox dbgFateCardId;
        private FlatTabControl.FlatTabControl tabCards;
        private System.Windows.Forms.TabPage tabCompanyCards;
        private System.Windows.Forms.TabPage tabPrivateCompanyCards;
        private System.Windows.Forms.TabPage tabBondCards;
        private System.Windows.Forms.TabPage tabBankCards;
        private System.Windows.Forms.TabPage tabCarCards;
        private System.Windows.Forms.FlowLayoutPanel flowPrivateCompanyCards;
        private System.Windows.Forms.FlowLayoutPanel flowBondCards;
        private System.Windows.Forms.FlowLayoutPanel flowBankCards;
        private System.Windows.Forms.FlowLayoutPanel flowCarCards;
        private System.Windows.Forms.Panel pnlStockMarket;
        private System.Windows.Forms.FlowLayoutPanel flowCompanies1;
        private System.Windows.Forms.FlowLayoutPanel flowCompanies6;
        private System.Windows.Forms.FlowLayoutPanel flowCompanies5;
        private System.Windows.Forms.FlowLayoutPanel flowCompanies4;
        private System.Windows.Forms.FlowLayoutPanel flowCompanies3;
        private System.Windows.Forms.FlowLayoutPanel flowCompanies2;
    }
}
