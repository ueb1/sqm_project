﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Game.Controls;
using Game.Entities;
using Game.Entities.Cards;

namespace Game.Views
{
    public partial class WalletView : UserControl
    {
        private FlowLayoutPanel[] companyPanels;

        public WalletView()
        {
            InitializeComponent();

            companyPanels = new FlowLayoutPanel[6];
            companyPanels[0] = flowCompany1;
            companyPanels[1] = flowCompany2;
            companyPanels[2] = flowCompany3;
            companyPanels[3] = flowCompany4;
            companyPanels[4] = flowCompany5;
            companyPanels[5] = flowCompany6;
        }

        public void layoutCompanyCards(Wallet wallet)
        {
            int currentPanel = 0;
            int lastGroup = -1;
            foreach(KeyValuePair<Company, List<CompanyCard>> stack in wallet.CompanyCards)
            {
                if (lastGroup >= 0 && (int)stack.Key.BusinessGroup != lastGroup)
                    currentPanel++;
                if (stack.Value.Count > 0)
                {
                    companyPanels[currentPanel].Controls.Add(new CompanyCardSmallControl(stack.Value[0].Company, stack.Value.Count));
                    lastGroup = (int)stack.Key.BusinessGroup;
                }
            }
        }

        public void Clear()
        {
            for (int i = 0; i < companyPanels.Length; i++)
            {
                companyPanels[i].Controls.Clear();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
