﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Entities
{
    public class Company
    {
        #region Private members
        private int companyId;
        private BusinessGroup businessGroup;
        private string companyName;
        private int nrOfStaff;
        private Image imageLarge;
        private Image imageSmall;
        private Image imageTiny;
        #endregion

        #region Attributes
        public int CompanyId
        {
            get
            {
                return companyId;
            }
        }
        public BusinessGroup BusinessGroup
        {
            get
            {
                return businessGroup;
            }
        }
        public string CompanyName
        {
            get
            {
                return companyName;
            }
        }
        public int NrOfStaff
        {
            get
            {
                return nrOfStaff;
            }
        }
        public int CostEachShare
        {
            get
            {
                return Constants.COMPANY_SHARE_COST[(int)BusinessGroup];
            }
        }

        public Image ImageLarge
        {
            get
            {
                return imageLarge;
            }
        }
        public Image ImageSmall
        {
            get
            {
                return imageSmall;
            }
        }
        public Image ImageTiny
        {
            get
            {
                return imageTiny;
            }
        }
        public Color CompanyColor
        {
            get
            {
                return Constants.COMPANY_GROUP_COLOR[(int)businessGroup];
            }
        }

        public int Profit { get; set; }
        #endregion

        public Company(int companyId, string companyName, int nrOfStaff, BusinessGroup businessGroup, Image largeLogo, Image smallLogo, Image tinyLogo)
        {
            this.companyName = companyName;
            this.nrOfStaff = nrOfStaff;
            this.businessGroup = businessGroup;
            this.companyId = companyId;
            Profit = 0;
            imageLarge = largeLogo;
            imageSmall = smallLogo;
            imageTiny = tinyLogo;
        }
    }
}
