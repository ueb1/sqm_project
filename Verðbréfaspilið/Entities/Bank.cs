﻿using Game.Entities.Cards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Entities
{
    public class Bank
    {
        private List<ExchangeCard> exchangeRateTables;

        #region Attributes
        public Wallet Wallet { get; set; }
        public ExchangeCard ExchangeTable
        {
            get
            {
                return exchangeRateTables[0];
            }
        }
        #endregion
        public event EventHandler ShortOnFunds;
        #region Events

        #endregion

        public Bank(int funds, List<BankCard> bankCards, List<BondCard>[] bondCards, Dictionary<Company, List<CompanyCard>> companyCards, Dictionary<PrivateCompany, PrivateCompanyCard> privateCompanyCards, List<FundLetterCard>[,] fundLetterCards, List<CarCard> carCards)
        {
            Wallet = new Wallet(funds, bankCards, bondCards, companyCards, privateCompanyCards, fundLetterCards, carCards);
            Random rnd = new Random();
            shuffleExchangeRateTables();
        }

        public Bank(Wallet wallet)
        {
            Wallet = wallet;
        }

        private void shuffleExchangeRateTables()
        {
            exchangeRateTables = new List<ExchangeCard>();
            for (int i = 0; i < Constants.STOCK_RATES.GetLength(0); i++)
            {

                int[] stockRates = new int[Constants.STOCK_RATES.GetLength(1)];
                decimal[] fundLetterRates = new decimal[Constants.FUNDLETTER_RATES.GetLength(1)];
                for (int j = 0; j < stockRates.Length; j++)
                {
                    stockRates[j] = Constants.STOCK_RATES[i, j];
                }
                for (int j = 0; j < fundLetterRates.Length; j++)
                {
                    fundLetterRates[j] = Constants.FUNDLETTER_RATES[i, j];
                }
                exchangeRateTables.Add(new ExchangeCard(stockRates, fundLetterRates, Constants.INTEREST_RATES[i]));
            }
            
            exchangeRateTables.Shuffle();
        }

        public void ChangeExchangeRate()
        {
            if (exchangeRateTables.Count == 0)
            {
                shuffleExchangeRateTables();
            }
            else
                exchangeRateTables.RemoveAt(0);
        }
        public bool GiveCard(Card card, Player player)
        {
            if (Wallet.Remove(card))
            {
                if (player.Wallet.Add(card))
                    return true;
                else
                    Wallet.Add(card); // If we fail to add card to the other wallet we return it to this wallet
            }
            return false;
        }

        public void Pay(int amount, Player player)
        {
            if (Wallet.Cash < amount)
            {
                ShortOnFunds.Invoke(this, null);
            }
            else
            {
                Wallet.Cash -= amount;
                player.Wallet.Cash += amount;
            }
        }

        public void Pay(int amount)
        {
            if (Wallet.Cash < amount)
            {
                ShortOnFunds.Invoke(this, null);
            }
            else
            {
                Wallet.Cash -= amount;
            }
        }

        #region Inquiry methods
        public bool HasShare(Company company)
        {
            return Wallet.CompanyCards[company].Count > 0;
        }
        public bool HasShare(PrivateCompany company)
        {
            return Wallet.PrivateCompanyCards[company] != null;
        }
        public int NrOfShares(Company company)
        {
            return Wallet.CompanyCards[company].Count;
        }
        public CompanyCard GetShare(Company company)
        {
            if (HasShare(company))
            {
                return Wallet.CompanyCards[company][0];
            }
            return null;
        }
        public PrivateCompanyCard GetShare(PrivateCompany company)
        {
            return Wallet.PrivateCompanyCards[company]; // Dont need to check if exists, returns null anyway if not exist
        }
        public int NrOfPrivateCompanies()
        {
            return Wallet.PrivateCompanyCards.Count(o => o.Value != null);
        }
        public int NrOfCars()
        {
            return Wallet.CarCards.Count;
        }
        public CarCard GetCarCard(Car car)
        {
            return Wallet.CarCards.Find(o => o.Car == car);
        }
        public List<CarCard> GetCarCards()
        {
            return Wallet.CarCards;
        }
        #endregion
    }
}
