﻿using Game.Entities.Cards;
using Game.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Entities
{
    public class Wallet
    {
        public List<BankCard> BankCards { get; set; }
        public List<BondCard>[] BondCards { get; set; }
        public Dictionary<Company, List<CompanyCard>> CompanyCards { get; set; }
        public List<FundLetterCard>[,] FundLetterCards { get; set; }
        public Dictionary<PrivateCompany, PrivateCompanyCard> PrivateCompanyCards { get; set; }
        public int Cash { get; set; }
        public List<CarCard> CarCards { get; set; }

        public Wallet()
        {
            BankCards = new List<BankCard>();
            BondCards = new List<BondCard>[2];
            BondCards[0] = new List<BondCard>();
            BondCards[1] = new List<BondCard>();
            CompanyCards = new Dictionary<Company, List<CompanyCard>>();
            for (int i = 0; i < Constants.COMPANIES.Length; i++)
            {
                CompanyCards.Add(Constants.COMPANIES[i], new List<CompanyCard>());
            }
            PrivateCompanyCards = new Dictionary<PrivateCompany, PrivateCompanyCard>();
            for (int i = 0; i < Constants.PRIVATE_COMPANIES.Length; i++)
            {
                PrivateCompanyCards.Add(Constants.PRIVATE_COMPANIES[i], null);
            }
            FundLetterCards = new List<FundLetterCard>[Enum.GetNames(typeof(FundLetterCardGroup)).Length, Constants.NR_OF_FUND_LETTER_TYPES_PER_GROUP];
            for (int i = 0; i < 3; i++)
            {
                FundLetterCards[0, i] = new List<FundLetterCard>();
            }
            for (int i = 0; i < 3; i++)
            {
                FundLetterCards[1, i] = new List<FundLetterCard>();
            }
            CarCards = new List<CarCard>();
        }

        public Wallet(int funds) : this()
        {
            this.Cash = funds;

        }
        public Wallet(int funds, List<BankCard> bankCards, List<BondCard>[] bondCards, Dictionary<Company, List<CompanyCard>> companyCards, Dictionary<PrivateCompany, PrivateCompanyCard> privateCompanyCards, List<FundLetterCard>[,] fundLetterCards, List<CarCard> carsCards)
        {
            this.Cash = funds;
            this.BankCards = bankCards;
            this.BondCards = bondCards;
            this.CompanyCards = companyCards;
            this.FundLetterCards = fundLetterCards;
            this.PrivateCompanyCards = privateCompanyCards;
            this.CarCards = carsCards;
        }

        public bool Contains(Card card)
        {
            bool containsCard = false;

            if (card is BankCard)
            {
                containsCard = BankCards.Contains(card);
            }
            else if (card is BondCard)
            {
                BondCard thisCard = (BondCard)card;
                containsCard = BondCards[(int)thisCard.Group].Contains(card);
            }
            else if (card is CompanyCard)
            {
                CompanyCard thisCard = (CompanyCard)card;
                containsCard = CompanyCards[thisCard.Company].Contains(card);
            }
            else if (card is FundLetterCard)
            {
                FundLetterCard thisCard = (FundLetterCard)card;
                containsCard = FundLetterCards[(int)thisCard.Group, thisCard.ClassNr].Contains(card);
            }
            else if (card is PrivateCompanyCard)
            {
                PrivateCompanyCard thisCard = (PrivateCompanyCard)card;
                containsCard = PrivateCompanyCards[thisCard.Company] != null;
            }
            else if(card is CarCard)
            {
                containsCard = CarCards.Contains(card);
            }

            return containsCard;
        }

        public bool Add(Card card)
        {
            if (!Contains(card))
            {
                if (card is BankCard)
                {
                    BankCard thisCard = (BankCard)card;
                    BankCards.Add(thisCard);
                }
                else if (card is BondCard)
                {
                    BondCard thisCard = (BondCard)card;
                    BondCards[(int)thisCard.Group].Add(thisCard);
                }
                else if (card is CompanyCard)
                {
                    CompanyCard thisCard = (CompanyCard)card;
                    CompanyCards[thisCard.Company].Add(thisCard);
                }
                else if (card is FundLetterCard)
                {
                    FundLetterCard thisCard = (FundLetterCard)card;
                    FundLetterCards[(int)thisCard.Group, thisCard.ClassNr].Add(thisCard);
                }
                else if (card is PrivateCompanyCard)
                {
                    PrivateCompanyCard thisCard = (PrivateCompanyCard)card;
                    PrivateCompanyCards[thisCard.Company] = thisCard;
                }
                else if (card is CarCard)
                {
                    CarCard thisCard = (CarCard)card;
                    CarCards.Add(thisCard);
                }
                return true;
            }
            else
                return false;
        }

        public bool Remove(Card card)
        {
            if (Contains(card))
            {
                if (card is BankCard)
                {
                    BankCard thisCard = (BankCard)card;
                    BankCards.Remove(thisCard);
                }
                else if (card is BondCard)
                {
                    BondCard thisCard = (BondCard)card;
                    BondCards[(int)thisCard.Group].Remove(thisCard);
                }
                else if (card is CompanyCard)
                {
                    CompanyCard thisCard = (CompanyCard)card;
                    CompanyCards[thisCard.Company].Remove(thisCard);
                }
                else if (card is FundLetterCard)
                {
                    FundLetterCard thisCard = (FundLetterCard)card;
                    FundLetterCards[(int)thisCard.Group, thisCard.ClassNr].Remove(thisCard);
                }
                else if (card is PrivateCompanyCard)
                {
                    PrivateCompanyCard thisCard = (PrivateCompanyCard)card;
                    PrivateCompanyCards[thisCard.Company] = null;
                }
                else if (card is CarCard)
                {
                    CarCard thisCard = (CarCard)card;
                    CarCards.Remove(thisCard);
                }
                return true;
            }
            else
                return false;
        }

        public List<Card> GetCardList()
        {
            throw new NotImplementedException();
        }

        public List<CompanyCard> GetCompanyCardList(int group = -1)
        {
            List<CompanyCard> list = new List<CompanyCard>();
            foreach(KeyValuePair<Company, List<CompanyCard>> company in CompanyCards)
            {
                list.AddRange(company.Value);
            }

            return list;
        }

        public CompanyCard GetCompanyCard(Company company)
        {
            if(CompanyCards[company].Count > 0)
            {
                return CompanyCards[company].First();
            }
            else
                return null;
        }

        public int GetNumberOfShares(Company company)
        {
            return CompanyCards[company].Count;
        }

        public int GetTotalAsset()
        {
            int asset = 0;
            foreach(BankCard card in BankCards)
            {
                if(card.IsDeposit)
                {
                    asset += card.Amount;
                }
            }
            for(int i=0; i< Enum.GetNames(typeof(BondCardGroup)).Length; i++)
            {
                foreach(BondCard card in BondCards[i])
                {
                    asset += card.Amount;
                }
            }
            foreach(CarCard card in CarCards)
            {
                asset += card.Car.Price;
            }
            for(int i=0; i<Constants.COMPANIES.Length; i++)
            {
                foreach(CompanyCard card in CompanyCards[Constants.COMPANIES[i]])
                {
                    asset += card.ShareValue;
                }
            }
            for(int i=0; i< Enum.GetNames(typeof(FundLetterCardGroup)).Length; i++)
            {
                for(int j=0; j<Constants.NR_OF_FUND_LETTER_TYPES_PER_GROUP; j++)
                {
                    foreach(FundLetterCard card in FundLetterCards[i, j])
                    {
                        asset += card.Amount;
                    }
                }
            }

            for(int i=0; i<Constants.PRIVATE_COMPANIES.Length; i++)
            {
                if(PrivateCompanyCards[Constants.PRIVATE_COMPANIES[i]] != null)
                asset += PrivateCompanyCards[Constants.PRIVATE_COMPANIES[i]].Value;
            }
            asset += Cash;

            return asset;
        }
    }
}
