﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Entities
{
    public class PrivateCompany
    {
        #region Private members
        private int companyId;
        private string companyName;
        private int nrOfStaff;
        private int value;
        private int[] dividends;
        private int profit;
        private KeyValuePair<PrivateCompany, int> conditionalDividend;
        private Image imageLarge;
        private Image imageSmall;
        private Image imageTiny;
        #endregion

        #region Attributes
        public int CompanyId
        {
            get
            {
                return companyId;
            }
        }
        public string CompanyName
        {
            get
            {
                return companyName;
            }
        }
        public int NrOfStaff
        {
            get
            {
                return nrOfStaff;
            }
        }
        public int Value
        {
            get
            {
                return value;
            }
        }
        public int Profit { get; set; }

        public KeyValuePair<PrivateCompany, int> ConditionalDividend
        {
            get
            {
                return conditionalDividend;
            }
        }

        public Image ImageLarge
        {
            get
            {
                return imageLarge;
            }
        }
        public Image ImageSmall
        {
            get
            {
                return imageSmall;
            }
        }
        public Image ImageTiny
        {
            get
            {
                return imageTiny;
            }
        }
        public Color CompanyColor
        {
            get
            {
                return Color.White;
            }
        }
        #endregion

        public PrivateCompany(int companyId, string companyName, int nrOfStaff, int value, int[] dividends, Image largeLogo, Image smallLogo, Image tinyLogo)
        {
            this.companyId = companyId;
            this.companyName = companyName;
            this.nrOfStaff = nrOfStaff;
            this.value = value;
            this.dividends = dividends;
            imageLarge = largeLogo;
            imageSmall = smallLogo;
            imageTiny = tinyLogo;
        }

        public PrivateCompany(int companyId, string companyName, int nrOfStaff, int value, int[] dividends, KeyValuePair<PrivateCompany, int> conditionalDividend, Image largeLogo, Image smallLogo, Image tinyLogo)
        {
            this.companyId = companyId;
            this.companyName = companyName;
            this.nrOfStaff = nrOfStaff;
            this.value = value;
            this.dividends = dividends;
            this.conditionalDividend = conditionalDividend;
            imageLarge = largeLogo;
            imageSmall = smallLogo;
            imageTiny = tinyLogo;
        }

        public int GetDividend(List<PrivateCompany> ownedPrivateCompanies, int totalNumberOfPrivateCompanies)
        {
            int dividend = dividends[0];
            if (ownedPrivateCompanies.Count == totalNumberOfPrivateCompanies)
                dividend = dividends[dividends.Length - 1];
            else
            {
                if (conditionalDividend.Key != null && ownedPrivateCompanies.Contains(conditionalDividend.Key) && conditionalDividend.Value > dividend)
                {
                    dividend = conditionalDividend.Value;
                }
                if (ownedPrivateCompanies.Count >= Constants.NR_OF_REQUIRED_PRIVATE_COMPANIES_FOR_HIGHER_DIVIDEND && dividend < dividends[1])
                {
                    dividend = dividends[1];
                }
            }

            return dividend;
        }
    }
}
