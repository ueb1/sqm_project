﻿using Game.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Entities
{
    public class Car
    {
        #region Private members
        private string name;
        private int price;
        private CarCategory category;
        private string gearBox;
        private int weight;
        private decimal mileage;
        private int engineSize;
        private int enginePower;
        #endregion

        #region Attributes

        public string Name
        {
            get
            {
                return name;
            }
        }

        public int Price
        {
            get
            {
                return price;
            }
        }

        public CarCategory Category
        {
            get
            {
                return category;
            }
        }

        public string GearBox
        {
            get
            {
                return gearBox;
            }
        }

        public int Weight
        {
            get
            {
                return weight;
            }
        }

        public decimal Mileage
        {
            get
            {
                return mileage;
            }
        }

        public int EngineSized
        {
            get
            {
                return engineSize;
            }
        }

        public int EnginePower
        {
            get
            {
                return enginePower;
            }
        }
        #endregion

        public Car(string name, int price, CarCategory category, string gearBox, int weight, decimal mileage, int engineSize, int enginePower)
        {
            this.name = name;
            this.price = price;
            this.category = category;
            this.gearBox = gearBox;
            this.weight = weight;
            this.mileage = mileage;
            this.engineSize = engineSize;
            this.enginePower = enginePower;
        }
    }
}
