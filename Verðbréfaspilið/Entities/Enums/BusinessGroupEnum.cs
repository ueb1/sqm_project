﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Entities
{
    public enum BusinessGroup
    {
        Green = 0,
        Orange = 1,
        Pink = 2,
        Tan = 3,
        Purple = 4,
        DarkGreen = 5
    }
}
