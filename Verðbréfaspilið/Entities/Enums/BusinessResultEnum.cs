﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Entities.Enums
{
    public enum BusinessResultEnum
    {
        OK = 0,
        NotEnoughFunds = 1,
        IllegalNumberOfShares = 2,
        NotEnoughShares = 3,
        CardUnavailable = 4
    }
}
