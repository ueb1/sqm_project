﻿using Game.Entities.Cards;
using Game.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Entities
{
    public class Player
    {
        #region Attributes
        public Wallet Wallet { get; set; }
        public string Name { get; set; }
        public bool MissesNextTurn { get; set; }
        public int TotalAssets
        {
            get
            {
                return Wallet.GetTotalAsset();
            }
        }
        public decimal TotalFunds
        {
            get
            {
                return Wallet.Cash;
            }
        }
        #endregion

        #region Events
        public event EventHandler ShortOnFunds;
        public event EventHandler Bankruptcy;
        #endregion

        public Player(string name)
        {
            Name = name;
        }

        public Player(string name, int funds)
        {
            Name = name;
            Wallet = new Wallet();
            Wallet.Cash = funds;
        }

        public Player(string name, int funds, CarCard carCard)
        {
            Name = name;
            Wallet = new Wallet();
            Wallet.Cash = funds;
            Wallet.CarCards.Add(carCard);
        }

        public Player(string name, int funds, List<BankCard> bankCards, List<BondCard>[] bondCards, Dictionary<Company, List<CompanyCard>> companyCards, Dictionary<PrivateCompany, PrivateCompanyCard> privateCompanyCards, List<FundLetterCard>[,] fundLetterCards, List<CarCard> carCards)
        {
            Name = name;
            Wallet = new Wallet(funds, bankCards, bondCards, companyCards, privateCompanyCards, fundLetterCards, carCards);
        }

        public Player(string name, Wallet wallet)
        {
            Name = name;
            Wallet = wallet;
        }

        public bool Give(Card card, Player otherPlayer)
        {
            if (Wallet.Remove(card))
            {
                if (otherPlayer.Wallet.Add(card))
                    return true;
                else
                    Wallet.Add(card); // If we fail to add card to the other wallet we return it to this wallet
            }
            return false;
        }

        public bool Give(Card card, Bank bank)
        {
            if (Wallet.Remove(card))
            {
                if (bank.Wallet.Add(card))
                    return true;
                else
                    Wallet.Add(card); // If we fail to add card to the other wallet we return it to this wallet
            }
            return false;
        }

        public void Pay(int amount, Bank bank)
        {
            if(Wallet.Cash < amount)
            {
                ShortOnFundsEventArgs args = new ShortOnFundsEventArgs(amount, true);
                ShortOnFunds.Invoke(this, args);

                if (Wallet.Cash >= amount)
                {
                    Pay(amount, bank);

                }
                else
                {
                    int fundsLeft = Wallet.Cash;
                    Pay(fundsLeft, bank);
                    Bankruptcy.Invoke(this, null);
                }
            }
            else
            {
                Wallet.Cash -= amount;
                bank.Wallet.Cash += amount;
            }
        }

        public void Pay(int amount, Player player)
        {
            if (Wallet.Cash < amount)
            {
                ShortOnFundsEventArgs args = new ShortOnFundsEventArgs(amount, player);
                ShortOnFunds.Invoke(this, args);

                if (Wallet.Cash >= amount)
                {
                    Pay(amount, player);
                }
                else
                {
                    int fundsLeft = Wallet.Cash;
                    Pay(fundsLeft, player);
                    Bankruptcy.Invoke(this, null);
                }
            }
            else
            {
                Wallet.Cash -= amount;
                player.Wallet.Cash += amount;
            }
        }

        public int Pay(int amount)
        {

            if (Wallet.Cash < amount)
            {
                ShortOnFundsEventArgs args = new ShortOnFundsEventArgs(amount, false);
                ShortOnFunds.Invoke(this, args);
                if(Wallet.Cash >= amount)
                {
                    return Pay(amount);
                }
                else
                {
                    int fundsLeft = Wallet.Cash;
                    Bankruptcy.Invoke(this, null);
                    return Pay(fundsLeft);
                }
            }
            else
            {
                Wallet.Cash -= amount;
                return amount;
            }
        }

        public BusinessResultEnum Buy(Bank bank, PrivateCompany company)
        {
            BusinessResultEnum result = BusinessResultEnum.OK;
            // If player is allowed to buy that many shares
            if (bank.HasShare(company))
            {
                    // If the player can afford the cost
                    if (Wallet.Cash >= company.Value)
                    {
                        Pay(company.Value, bank);
                        bank.GiveCard(bank.GetShare(company), this);
                    }
                    else
                        result = BusinessResultEnum.NotEnoughFunds;
            }
            else
                result = BusinessResultEnum.NotEnoughShares;

            return result;
        }

        public BusinessResultEnum Buy(Bank bank, Company company, int numberOfShares)
        {
            BusinessResultEnum result = BusinessResultEnum.OK;
            // If player is allowed to buy that many shares
            if (numberOfShares <= bank.ExchangeTable.StockRates[(int)company.BusinessGroup])
            {
                // If the bank has enough shares available
                if (bank.NrOfShares(company) >= numberOfShares)
                {
                    int totalCost = company.CostEachShare * bank.ExchangeTable.StockRates[(int)company.BusinessGroup] * numberOfShares;
                    // If the player can afford the cost
                    if (Wallet.Cash >= totalCost)
                    {
                        Pay(totalCost, bank);
                        for (int i = 0; i < numberOfShares == true; i++)
                        {
                            bank.GiveCard(bank.GetShare(company), this);
                        }
                    }
                    else
                        result = BusinessResultEnum.NotEnoughFunds;
                }
                else
                    result = BusinessResultEnum.NotEnoughShares;
            }
            else
                result = BusinessResultEnum.IllegalNumberOfShares;

            return result;
        }

        public BusinessResultEnum Buy(Bank bank, Car car)
        {
            BusinessResultEnum result = BusinessResultEnum.OK;
            // If the bank has this car
                CarCard carCard = bank.Wallet.CarCards.Find(o => o.Car == car);
                if (carCard != null)
                {
                    // If the player can afford the car
                    if (Wallet.Cash >= car.Price)
                    {
                        Pay(car.Price, bank);
                        bank.GiveCard(carCard, this);
                    }
                    else
                        result = BusinessResultEnum.NotEnoughFunds;
                }
                else
                    result = BusinessResultEnum.CardUnavailable;

            return result;
        }

        #region Inquiry methods
        public bool HasShare(Company company)
        {
            return Wallet.CompanyCards[company].Count > 0;
        }
        public bool HasShare(PrivateCompany company)
        {
            return Wallet.PrivateCompanyCards[company] != null;
        }
        public CompanyCard GetShare(Company company)
        {
            if(HasShare(company))
            {
                return Wallet.CompanyCards[company][0];
            }
            return null;
        }
        public PrivateCompanyCard GetShare(PrivateCompany company)
        {
            return Wallet.PrivateCompanyCards[company]; // Dont need to check if exists, returns null anyway if not exist
        }
        public int NrOfShares(Company company)
        {
            return Wallet.CompanyCards[company].Count;
        }
        public int NrOfPrivateCompanies()
        {
            return Wallet.PrivateCompanyCards.Count(o => o.Value != null);
        }
        public int NrOfCars()
        {
            return Wallet.CarCards.Count;
        }
        public CarCard GetCarCard(Car car)
        {
            return Wallet.CarCards.Find(o => o.Car == car);
        }
        public List<CarCard> GetCarCards()
        {
            return Wallet.CarCards;
        }
        #endregion
    }
}
