﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Entities.Cards
{
    public class CarCard : Card
    {
        #region Private members
        private Car car;
        #endregion

        #region Attributes

        #endregion
        public Car Car
        {
            get
            {
                return car;
            }
        }

        public CarCard(Car car) : base(car.Name)
        {
            this.car = car;
        }
    }
}
