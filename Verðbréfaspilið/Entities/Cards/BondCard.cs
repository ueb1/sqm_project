﻿using Game.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Entities.Cards
{
    public class BondCard : Card
    {
        #region Private members
        private string name;
        private int amount;
        private BondCardGroup group;
        #endregion

        #region Attributes
        public string Name
        {
            get
            {
                return name;
            }
        }

        public int Amount
        {
            get
            {
                return amount;
            }
        }
        public BondCardGroup Group
        {
            get
            {
                return group;
            }
        }
        #endregion

        public BondCard(int amount, BondCardGroup group) : base("Spariskírteini Ríkissjóðs")
        {
            this.name = "Spariskírteini Ríkissjóðs";
            this.amount = amount;
            this.group = group;
        }
    }
}
