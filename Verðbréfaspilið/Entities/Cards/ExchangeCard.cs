﻿using Game.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Entities.Cards
{
    public class ExchangeCard : Card
    {
        #region Private members
        private int[] stockRates;
        private decimal[] fundLetterRates;
        private decimal interestRate;
        #endregion

        #region Attributes
        public int[] StockRates
        {
            get
            {
                return stockRates;
            }
        }

        public decimal[] FundLetterRates
        {
            get
            {
                return fundLetterRates;
            }
        }

        public decimal InterestRate
        {
            get
            {
                return interestRate;
            }
        }
        #endregion

        public ExchangeCard(int[] stockRates, decimal[] fundLetterRates, decimal interestRates) : base("Gengistafla")
        {
            this.stockRates = stockRates;
            this.fundLetterRates = fundLetterRates;
            this.interestRate = interestRates;
        }

        public decimal GetStockRates(BusinessGroup group)
        {
            return stockRates[(int)group];
        }

        public decimal GetFundLetterRates(FundLetterCardGroup group)
        {
            return fundLetterRates[(int)group];
        }
    }
}
