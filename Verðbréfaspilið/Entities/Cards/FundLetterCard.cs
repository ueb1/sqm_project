﻿using Game.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Entities.Cards
{
    public class FundLetterCard : Card
    {
        #region Private members
        private string name;
        private string issuer;
        private int amount;
        private int classNr;
        private FundLetterCardGroup group;
        #endregion

        #region Attributes
        public int ClassNr
        {
            get
            {
                return classNr;
            }
        }
        public string Name
        {
            get
            {
                return name;
            }
        }

        public string Issuer
        {
            get
            {
                return issuer;
            }
        }

        public int Amount
        {
            get
            {
                return amount;
            }
        }

        public FundLetterCardGroup Group
        {
            get
            {
                return group;
            }
        }
        #endregion

        public FundLetterCard(string name, string issuer, int amount, FundLetterCardGroup group, int classNr) : base(name)
        {
            this.name = name;
            this.issuer = issuer;
            this.amount = amount;
            this.group = group;
            this.classNr = classNr;
        }
    }
}
