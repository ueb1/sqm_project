﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Entities.Cells
{
    public class CompanyCell : Cell
    {
        #region Private members
        private Company company;
        private bool isStockMarket;
        #endregion

        #region Attributes
        public Company Company
        {
            get
            {
                return company;
            }
        }

        public bool IsStockMarket
        {
            get
            {
                return isStockMarket;
            }
        }
        #endregion

        public CompanyCell(int x, int y, int width, int height, bool changeExchangeRate, Company company, bool isStockMarket = false) : base(x, y, width, height, changeExchangeRate)
        {
            InitializeComponent();

            this.company = company;
            this.isStockMarket = isStockMarket;
            this.BackColor = company.CompanyColor;
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // CompanyCell
            // 
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.CompanyCell_Paint);
            this.ResumeLayout(false);

        }

        private void CompanyCell_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            e.Graphics.DrawImage(company.ImageTiny, 0, (Height - company.ImageTiny.Height) / 2, company.ImageTiny.Width, company.ImageTiny.Height);
            if (isStockMarket)
            {
                e.Graphics.FillRectangle(Brushes.Blue, 0, 0, 8, 8);
                e.Graphics.FillRectangle(Brushes.Blue, Width - 8, 0, 8, 8);
                e.Graphics.FillRectangle(Brushes.Blue, 0, Height - 8, 8, 8);
                e.Graphics.FillRectangle(Brushes.Blue, Width - 8, Height - 8, 8, 8);
            }
        }
    }
}
