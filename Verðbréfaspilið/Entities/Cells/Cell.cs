﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Game.Entities.Cells
{
    // The parent class for all celltypes
    public class Cell : Panel
    {
        public Color Color { get; set; }
        public bool ChangeExchangeRate { get; set; }
        public List<PlayerPiece> PlayerPieces { get; set; }

        public Cell ()
        {

        }

        public Cell (int x, int y, int width, int height, bool changeExchangeRate)
        {
            Location = new Point(x, y);
            Size = new Size(width, height);
            Bounds = new Rectangle(Location, Size);
            BackColor = Color.Transparent;
            ChangeExchangeRate = changeExchangeRate;
            PlayerPieces = new List<PlayerPiece>();
        }
    }
}
