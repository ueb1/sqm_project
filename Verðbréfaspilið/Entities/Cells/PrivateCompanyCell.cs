﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Entities.Cells
{
    class PrivateCompanyCell : Cell
    {
        #region Private members
        private PrivateCompany company;
        #endregion

        #region Attributes
        public PrivateCompany Company
        {
            get
            {
                return company;
            }
        }

        #endregion

        public PrivateCompanyCell(int x, int y, int width, int height, bool changeExchangeRate, PrivateCompany company) : base(x, y, width, height, changeExchangeRate)
        {
            InitializeComponent();

            this.company = company;
            this.BackColor = company.CompanyColor;
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // PrivateCompanyCell
            // 
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.PrivateCompanyCell_Paint);
            this.ResumeLayout(false);

        }

        private void PrivateCompanyCell_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            e.Graphics.DrawImage(company.ImageTiny, 0, (Height - company.ImageTiny.Height) / 2, company.ImageTiny.Width, company.ImageTiny.Height);
        }
    }
}
