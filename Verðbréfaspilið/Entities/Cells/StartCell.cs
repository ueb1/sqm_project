﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Entities.Cells
{
    public class StartCell : Cell
    {
        #region Private members

        #endregion

        #region Attributes

        #endregion

        public StartCell(int x, int y, int width, int height, bool changeExchangeRate) : base(x, y, width, height, changeExchangeRate)
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // StartCell
            // 
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.StartCell_Paint);
            this.ResumeLayout(false);

        }

        private void StartCell_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            e.Graphics.DrawImage(Properties.Resources.startCell, 0, 0, Width, Height);

        }
    }
}
