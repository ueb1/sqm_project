# Contributions to the project
This project is open for contributions given that each individual conforms to the coding standards listed 
in the projects [wiki page](https://gitlab.com/ueb1/sqm_project/-/wikis/Software%20Quality%20Management%20Plan)