﻿namespace Game.Views
{
    partial class WalletView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowCompany1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowCompany3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowCompany5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowCompany2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowCompany4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowCompany6 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // flowCompany1
            // 
            this.flowCompany1.Location = new System.Drawing.Point(53, 3);
            this.flowCompany1.Name = "flowCompany1";
            this.flowCompany1.Size = new System.Drawing.Size(580, 162);
            this.flowCompany1.TabIndex = 10;
            // 
            // flowCompany3
            // 
            this.flowCompany3.Location = new System.Drawing.Point(53, 171);
            this.flowCompany3.Name = "flowCompany3";
            this.flowCompany3.Size = new System.Drawing.Size(580, 162);
            this.flowCompany3.TabIndex = 11;
            // 
            // flowCompany5
            // 
            this.flowCompany5.Location = new System.Drawing.Point(53, 339);
            this.flowCompany5.Name = "flowCompany5";
            this.flowCompany5.Size = new System.Drawing.Size(580, 162);
            this.flowCompany5.TabIndex = 12;
            // 
            // flowCompany2
            // 
            this.flowCompany2.Location = new System.Drawing.Point(639, 3);
            this.flowCompany2.Name = "flowCompany2";
            this.flowCompany2.Size = new System.Drawing.Size(580, 162);
            this.flowCompany2.TabIndex = 11;
            // 
            // flowCompany4
            // 
            this.flowCompany4.Location = new System.Drawing.Point(639, 171);
            this.flowCompany4.Name = "flowCompany4";
            this.flowCompany4.Size = new System.Drawing.Size(580, 162);
            this.flowCompany4.TabIndex = 13;
            // 
            // flowCompany6
            // 
            this.flowCompany6.Location = new System.Drawing.Point(639, 339);
            this.flowCompany6.Name = "flowCompany6";
            this.flowCompany6.Size = new System.Drawing.Size(580, 162);
            this.flowCompany6.TabIndex = 14;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Location = new System.Drawing.Point(639, 507);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(580, 162);
            this.flowLayoutPanel1.TabIndex = 15;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(235, 592);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 16;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // WalletView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.flowCompany6);
            this.Controls.Add(this.flowCompany4);
            this.Controls.Add(this.flowCompany5);
            this.Controls.Add(this.flowCompany1);
            this.Controls.Add(this.flowCompany3);
            this.Controls.Add(this.flowCompany2);
            this.Name = "WalletView";
            this.Size = new System.Drawing.Size(1280, 780);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.FlowLayoutPanel flowCompany1;
        private System.Windows.Forms.FlowLayoutPanel flowCompany3;
        private System.Windows.Forms.FlowLayoutPanel flowCompany5;
        private System.Windows.Forms.FlowLayoutPanel flowCompany2;
        private System.Windows.Forms.FlowLayoutPanel flowCompany4;
        private System.Windows.Forms.FlowLayoutPanel flowCompany6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        public System.Windows.Forms.Button button1;
    }
}
