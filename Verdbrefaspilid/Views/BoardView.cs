﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Game.Entities.Cells;
using Game.Entities;
using Game.Dialogs;
using Game.Entities.Cards;
using Game.Controls;

namespace Game.Views
{
    public partial class BoardView : UserControl
    {
        public event EventHandler MoveFinished;
        public event EventHandler<int> DebugActivateFateCard;
        public event EventHandler DebugActivateGovernmentCard;

        public StartCell StartCell { get; set; }
        public Cell[] Cells { get; set; }
        public PlayerPiece[] playerPieces;
        public Die[] dice;
        public int currentPlayerPiece;
        private List<Image> availableCarPieces;
        private Image[] dieImages;
        private int rollCounter;
        private int moveCounter;
        private Bank bank;
        private Dictionary<Company, CompanyCardTinyControl> companyCardControls;
        private bool diceLocked;
        private bool debugMode;

        public BoardView(bool debugMode)
        {
            InitializeComponent();
            this.debugMode = debugMode;
            if(debugMode)
            {
                numMovement.Visible = true;
                picDie1.Visible = false;
                picDie2.Visible = false;
                dbgFateCardId.Visible = true;
                dbgActivateFateCard.Visible = true;

            }
        }

        public void Init(Player[] players, Bank bank)
        {
            availableCarPieces = new List<Image>();
            availableCarPieces.Add(Properties.Resources.blueCar);
            availableCarPieces.Add(Properties.Resources.greenCar);
            availableCarPieces.Add(Properties.Resources.redCar);
            availableCarPieces.Add(Properties.Resources.tealCar);
            dieImages = new Image[6];
            dieImages[0] = Properties.Resources.Die1;
            dieImages[1] = Properties.Resources.Die2;
            dieImages[2] = Properties.Resources.Die3;
            dieImages[3] = Properties.Resources.Die4;
            dieImages[4] = Properties.Resources.Die5;
            dieImages[5] = Properties.Resources.Die6;
            dice = new Die[2];
            for (int i = 0; i < dice.Length; i++)
            {
                dice[i] = new Die();
            }
            dice[0].Roll();
            dice[1].Roll();
            picDie1.Image = dieImages[dice[0].Value - 1];
            picDie2.Image = dieImages[dice[1].Value - 1];

            rollCounter = 0;
            moveCounter = 0;

            StartCell = new StartCell(171, 234, 54, 54, false);
            pnlBoard.Controls.Add(StartCell);
            StartCell.Paint += Cell_Paint;
            int idx = 0;
            Cells = new Cell[55];
            Cells[idx++] = new PrivateCompanyCell(115, 234, 54, 54, false, Constants.PRIVATE_COMPANIES[(int)Constants.CompanyNamesEnum.CrownChicken]);
            Cells[idx++] = new FateCell(115, 177, 54, 54, false);
            Cells[idx++] = new CompanyCell(58, 145, 54, 54, false, Constants.COMPANIES[(int)Constants.CompanyNamesEnum.Ofnasmidjan]);
            Cells[idx++] = new CompanyCell(58, 88, 54, 54, false, Constants.COMPANIES[(int)Constants.CompanyNamesEnum.Bunadarbankinn], true);
            Cells[idx++] = new CompanyCell(58, 31, 54, 54, false, Constants.COMPANIES[(int)Constants.CompanyNamesEnum.Radiobudin]);
            Cells[idx++] = new FateCell(115, 31, 54, 54, false);
            Cells[idx++] = new CompanyCell(172, 31, 54, 54, false, Constants.COMPANIES[(int)Constants.CompanyNamesEnum.Esso]);
            Cells[idx++] = new PrivateCompanyCell(229, 31, 54, 54, false, Constants.PRIVATE_COMPANIES[(int)Constants.CompanyNamesEnum.Holdur]);
            Cells[idx++] = new AuctionCell(286, 31, 54, 54, false);
            Cells[idx++] = new CompanyCell(343, 31, 54, 54, false, Constants.COMPANIES[(int)Constants.CompanyNamesEnum.Ispan]);
            Cells[idx++] = new FateCell(400, 31, 54, 54, false);
            Cells[idx++] = new CompanyCell(457, 31, 54, 54, false, Constants.COMPANIES[(int)Constants.CompanyNamesEnum.Glitnir]);
            Cells[idx++] = new GovernmentCell(514, 31, 54, 54, false);
            Cells[idx++] = new CompanyCell(571, 31, 54, 54, false, Constants.COMPANIES[(int)Constants.CompanyNamesEnum.TreX]);
            Cells[idx++] = new StockMarketCell(628, 31, 54, 54, false);
            Cells[idx++] = new CompanyCell(685, 31, 54, 54, false, Constants.COMPANIES[(int)Constants.CompanyNamesEnum.Kodak]);
            Cells[idx++] = new CompanyCell(742, 31, 54, 54, false, Constants.COMPANIES[(int)Constants.CompanyNamesEnum.Mona]);
            Cells[idx++] = new CompanyCell(799, 31, 54, 54, false, Constants.COMPANIES[(int)Constants.CompanyNamesEnum.Vifilfell]);
            Cells[idx++] = new FateCell(856, 31, 54, 54, false);
            Cells[idx++] = new CompanyCell(913, 31, 54, 54, false, Constants.COMPANIES[(int)Constants.CompanyNamesEnum.Hedinn]);
            Cells[idx++] = new PrivateCompanyCell(970, 31, 54, 54, false, Constants.PRIVATE_COMPANIES[(int)Constants.CompanyNamesEnum.Bioborgin]);
            Cells[idx++] = new CompanyCell(1027, 31, 54, 54, false, Constants.COMPANIES[(int)Constants.CompanyNamesEnum.Malning]);
            Cells[idx++] = new PrivateCompanyCell(1084, 31, 54, 54, false, Constants.PRIVATE_COMPANIES[(int)Constants.CompanyNamesEnum.Biohollin]);
            Cells[idx++] = new StockMarketCell(1141, 31, 54, 54, false);
            Cells[idx++] = new CompanyCell(1141, 88, 54, 54, false, Constants.COMPANIES[(int)Constants.CompanyNamesEnum.Strikid]);
            Cells[idx++] = new CompanyCell(1141, 145, 54, 54, false, Constants.COMPANIES[(int)Constants.CompanyNamesEnum.Grandi]);
            Cells[idx++] = new CompanyCell(1141, 202, 54, 54, false, Constants.COMPANIES[(int)Constants.CompanyNamesEnum.Saeplast]);
            Cells[idx++] = new AuctionCell(1141, 259, 54, 54, false);
            Cells[idx++] = new GovernmentCell(1141, 316, 54, 54, false);
            Cells[idx++] = new PrivateCompanyCell(1141, 373, 54, 54, true, Constants.PRIVATE_COMPANIES[(int)Constants.CompanyNamesEnum.SveinnBakari]);
            Cells[idx++] = new FateCell(1141, 430, 54, 54, false);
            Cells[idx++] = new StockMarketCell(1141, 487, 54, 54, false);
            Cells[idx++] = new CompanyCell(1084, 487, 54, 54, false, Constants.COMPANIES[(int)Constants.CompanyNamesEnum.Flugleidir]);
            Cells[idx++] = new CompanyCell(1027, 487, 54, 54, false, Constants.COMPANIES[(int)Constants.CompanyNamesEnum.Jofur]);
            Cells[idx++] = new CompanyCell(970, 487, 54, 54, false, Constants.COMPANIES[(int)Constants.CompanyNamesEnum.UrvalUtsyn]);
            Cells[idx++] = new CompanyCell(913, 487, 54, 54, false, Constants.COMPANIES[(int)Constants.CompanyNamesEnum.SjovaAlmennar]);
            Cells[idx++] = new FateCell(856, 487, 54, 54, false);
            Cells[idx++] = new CompanyCell(799, 487, 54, 54, false, Constants.COMPANIES[(int)Constants.CompanyNamesEnum.Bilanaust]);
            Cells[idx++] = new FateCell(742, 487, 54, 54, false);
            Cells[idx++] = new CompanyCell(685, 487, 54, 54, false, Constants.COMPANIES[(int)Constants.CompanyNamesEnum.OJK]);
            Cells[idx++] = new CompanyCell(628, 487, 54, 54, false, Constants.COMPANIES[(int)Constants.CompanyNamesEnum.Skeljungur]);
            Cells[idx++] = new CompanyCell(571, 487, 54, 54, false, Constants.COMPANIES[(int)Constants.CompanyNamesEnum.Islandsbanki], true);
            Cells[idx++] = new CompanyCell(514, 487, 54, 54, false, Constants.COMPANIES[(int)Constants.CompanyNamesEnum.Oddi]);
            Cells[idx++] = new GovernmentCell(457, 487, 54, 54, false);
            Cells[idx++] = new PrivateCompanyCell(400, 487, 54, 54, false, Constants.PRIVATE_COMPANIES[(int)Constants.CompanyNamesEnum.Penninn]);
            Cells[idx++] = new CompanyCell(343, 487, 54, 54, false, Constants.COMPANIES[(int)Constants.CompanyNamesEnum.Papco]);
            Cells[idx++] = new CompanyCell(286, 487, 54, 54, false, Constants.COMPANIES[(int)Constants.CompanyNamesEnum.Fron]);
            Cells[idx++] = new FateCell(229, 487, 54, 54, false);
            Cells[idx++] = new CompanyCell(172, 487, 54, 54, false, Constants.COMPANIES[(int)Constants.CompanyNamesEnum.Ellingsen]);
            Cells[idx++] = new StockMarketCell(115, 487, 54, 54, false);
            Cells[idx++] = new CompanyCell(58, 487, 54, 54, false, Constants.COMPANIES[(int)Constants.CompanyNamesEnum.Tollvorugeymslan]);
            Cells[idx++] = new AuctionCell(58, 430, 54, 54, false);
            Cells[idx++] = new CompanyCell(58, 373, 54, 54, false, Constants.COMPANIES[(int)Constants.CompanyNamesEnum.MAX]);
            Cells[idx++] = new CompanyCell(58, 316, 54, 54, true, Constants.COMPANIES[(int)Constants.CompanyNamesEnum.Eimskip]);
            Cells[idx++] = new GovernmentCell(115, 291, 54, 54, false);
            for (int i = 0; i < Cells.Length; i++)
            {
                pnlBoard.Controls.Add(Cells[i]);
                Cells[i].Paint += Cell_Paint;
            }

            playerPieces = new PlayerPiece[players.Length];
            for (int i = 0; i < players.Length; i++)
            {
                playerPieces[i] = new PlayerPiece();
                playerPieces[i].Player = players[i];
                playerPieces[i].CellNumber = -1;
                playerPieces[i].Icon = availableCarPieces[0];
                StartCell.PlayerPieces.Add(playerPieces[i]);
                availableCarPieces.RemoveAt(0);
            }
            this.bank = bank;

            companyCardControls = new Dictionary<Company, CompanyCardTinyControl>();
            foreach(Company comp in Constants.COMPANIES)
            {
                CompanyCardTinyControl c = new CompanyCardTinyControl(comp, 0);
                c.ControlClicked += tinyCompanyCard_click;
                companyCardControls.Add(comp, c);
                flowCompanyCards.Controls.Add(c);
            }

            exchangeRateCard.SetExchangeRate(bank.ExchangeTable);

            pnlStockMarket.Visible = false;
            pnlBoard.Visible = true;
            SetNextPlayer(0);
            RefreshBoard();
        }

        private void tinyCompanyCard_click(object sender, EventArgs e)
        {
            CompanyCardTinyControl control = (CompanyCardTinyControl)sender;
            CompanyCardDialog.ShowDialog(control.Company);
        }

        public void RefreshAssetsBoard(Wallet wallet)
        {
            lblCash.Text = playerPieces[currentPlayerPiece].Player.TotalFunds.ToString("N0");
            //flowLayoutPanel1.Controls.Clear();
            foreach (KeyValuePair<Company, List<CompanyCard>> stack in wallet.CompanyCards)
            {
                 companyCardControls[stack.Key].SetStackSize(stack.Value.Count);
            }
        }

        private void Cell_Paint(object sender, PaintEventArgs e)
        {
            Graphics g;
            Cell cell = (Cell)sender;
            int zValue = 1;
            foreach (PlayerPiece p in cell.PlayerPieces)
            {
                g = e.Graphics;
                int nrOfPiecesOnCell = cell.PlayerPieces.Count;
                g.DrawImage(p.Icon, cell.Width / 2 - p.Icon.Width / 2, cell.Height / 2 - p.Icon.Height / 2 - 3 * (nrOfPiecesOnCell - 1) + 6 * (zValue - 1));
                zValue++;
            }
        }

        public void RefreshBoard()
        {
            drawPieces();
        }

        private void drawPieces()
        {

        }

        private void picBoard_Click(object sender, EventArgs e)
        {
            if (debugMode)
            {
                movePiece((int)numMovement.Value);
            }
            else   
            {
                if (diceLocked == false)
                    RollDice();
            }
        }

        private void rollTimer_Tick(object sender, EventArgs e)
        {
            for (int i = 0; i < dice.Length; i++)
            {
                dice[i].Roll();
            }
            picDie1.Image = dieImages[dice[0].Value - 1];
            picDie2.Image = dieImages[dice[1].Value - 1];
            picDie1.Refresh();
            picDie2.Refresh();

            rollCounter++;

            if (rollCounter >= 8)
            {
                rollTimer.Stop();
                int diceValue = 0;
                for (int i = 0; i < dice.Length; i++)
                {
                    diceValue += dice[i].Value;
                }
                movePiece(diceValue);
            }
        }

        private void RollDice()
        {
            diceLocked = true;
            rollCounter = 0;
            rollTimer.Start();
        }

        private void movePiece(int distance)
        {
            moveCounter = distance;
            moveTimer.Start();
        }

        private void moveTimer_Tick(object sender, EventArgs e)
        {
            PlayerPiece piece = playerPieces[currentPlayerPiece];
            Cell currentCell;
            if (piece.CellNumber < 0)
                currentCell = StartCell;
            else
                currentCell = Cells[piece.CellNumber];

            if (moveCounter > 0)
            {
                int destination = (piece.CellNumber + 1) % Cells.Length;
                Cell nextCell = Cells[destination];

                if(nextCell.ChangeExchangeRate)
                {
                    bank.ChangeExchangeRate();
                    exchangeRateCard.SetExchangeRate(bank.ExchangeTable);
                }

                currentCell.PlayerPieces.Remove(piece);
                piece.Move(destination);
                Cells[destination].PlayerPieces.Add(piece);
                currentCell.Invalidate();
                nextCell.Invalidate();
                drawPieces();
                moveCounter--;
            }
            else
            {
                moveTimer.Stop();
                MoveFinished.Invoke(this, new PlayerMovedEventArgs(piece.Player, currentCell));
            }
        }

        public void SetNextPlayer(int playerNr)
        {
            currentPlayerPiece = playerNr;
            Player player = playerPieces[currentPlayerPiece].Player;
            lblPlayerName.Text = player.Name;
            RefreshAssetsBoard(player.Wallet);
            diceLocked = false;
        }

        private void dbgActivateFateCard_Click(object sender, EventArgs e)
        {
            if(int.TryParse(dbgFateCardId.Text, out int cardId))
            {
                DebugActivateFateCard.Invoke(this, cardId);
            }
        }

        public void OpenStockMarket(Bank bank)
        {
            pnlBoard.Visible = false;
            foreach(Company company in Constants.COMPANIES)
            {
                var c = new CompanyCardTinyControl(company, bank.NrOfShares(company));
                c.ControlClicked += tinyCompanyCard_click;
                switch (company.BusinessGroup)
                {
                    case BusinessGroup.Green:
                        flowCompanies1.Controls.Add(c);
                        break;
                    case BusinessGroup.Orange:
                        flowCompanies2.Controls.Add(c);
                        break;
                    case BusinessGroup.Pink:
                        flowCompanies3.Controls.Add(c);
                        break;
                    case BusinessGroup.Tan:
                        flowCompanies4.Controls.Add(c);
                        break;
                    case BusinessGroup.Purple:
                        flowCompanies5.Controls.Add(c);
                        break;
                    case BusinessGroup.DarkGreen:
                        flowCompanies6.Controls.Add(c);
                        break;
                }
                
            }

            pnlStockMarket.Visible = true;
        }

        public void CloseStockMarket()
        {
            pnlStockMarket.Visible = false;
            pnlBoard.Visible = true;
        }
    }
}
