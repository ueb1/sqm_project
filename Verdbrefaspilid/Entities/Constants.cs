﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Entities
{
    public static class Constants
    {
        public const int NR_OF_REQUIRED_PRIVATE_COMPANIES_FOR_HIGHER_DIVIDEND = 3;
        public const int PLAYER_STARTING_MONEY = 20000;
        public const int NR_COMPANY_SHARES = 4;
        public const int NR_OF_DICE = 2;

        public static readonly int[] COMPANY_SHARE_COST = { 500, 1000, 1500, 2000, 2500, 3000 };
        public static readonly int[,] COMPANY_DIVIDENDS = { { 50, 200, 500, 1000 }, { 100, 400, 1000, 2000 }, { 150, 600, 1500, 3000 }, { 200, 800, 2000, 4000 }, { 250, 1000, 2500, 5000 }, { 300, 1200, 3000, 6000 } };
        //public static readonly int[] INTEREST_RATES = { 5, 10, 20 };
        public static readonly int[] SHARE_MULTIPLIERS = { 1, 2, 3 };
        public const int BANK_CARD_COST = 3000;
        public const int BANK_LOAN_AMOUNT = 6000;
        public const int NR_OF_BANK_CARDS = 6;
        public static readonly int[] NR_OF_BOND_CARDS = { 6, 6 };
        public static readonly int[] BOND_CARD_COST = { 5000, 10000 };
        public const int NR_OF_FUND_LETTER_CARDS_PER_GROUP = 6;
        public static readonly int[,] FUND_LETTER_CARD_COST = { { 1000, 2000, 3000 }, { 4000, 5000, 6000 } };
        public const int NR_OF_FUND_LETTER_TYPES_PER_GROUP = 3;
        public static readonly Color[] COMPANY_GROUP_COLOR = { Color.LimeGreen, Color.DarkOrange, Color.Magenta, Color.NavajoWhite, Color.SlateBlue, Color.Teal};

        public static readonly int[,] STOCK_RATES =
        {
            { 1, 2, 2, 2, 3, 1 },
            { 2, 2, 2, 1, 1, 1 },
            { 2, 2, 3, 1, 1, 2 },
            { 1, 2, 1, 1, 2, 1 },
            { 3, 1, 1, 2, 2, 1 },
            { 2, 3, 1, 1, 2, 1 },
            { 1, 1, 2, 3, 1, 1 },
            { 2, 1, 1, 2, 2, 2 },
            { 1, 2, 1, 1, 1, 2 },
            { 1, 2, 1, 2, 1, 1 },
            { 2, 2, 1, 2, 1, 1 },
            { 2, 2, 1, 1, 2, 2 },
            { 1, 1, 2, 1, 1, 2 },
            { 1, 1, 2, 1, 1, 1 },
            { 2, 2, 2, 2, 1, 1 },
            { 2, 1, 1, 1, 2, 2 },
            { 1, 1, 1, 2, 2, 2 },
            { 2, 1, 2, 2, 1, 2 },
            { 1, 1, 2, 2, 1, 1 },
            { 2, 1, 2, 2, 1, 2 },
            { 2, 1, 2, 1, 2, 1 },
            { 1, 1, 2, 1, 2, 3 },
            { 1, 2, 1, 1, 2, 2 },
            { 1, 2, 1, 2, 1, 1 }
        };

        public static readonly decimal[,] FUNDLETTER_RATES =
        {
            { 1.5m, 1 },
            { 1.5m, 1 },
            { 1, 1 },
            { 1.5m, 1.5m },
            { 1, 1.5m },
            { 1.5m, 1 },
            { 1.5m, 1 },
            { 1.5m, 1.5m },
            { 0, 1.5m },
            { 1.5m, 1 },
            { 1, 0 },
            { 1, 1.5m },
            { 1, 0 },
            { 1.5m, 1.5m },
            { 1.5m, 1 },
            { 1, 1.5m },
            { 1, 1.5m },
            { 1.5m, 1.5m },
            { 1, 0 },
            { 0, 1 },
            { 0, 0 },
            { 1, 1 },
            { 1, 1 },
            { 0, 1.5m }
        };

        public static readonly decimal[] INTEREST_RATES = { 0.2m, 0.1m, 0.1m, 0.2m, 0.2m, 0.1m, 0.05m, 0.05m, 0.1m, 0.05m, 0.2m, 0.2m, 0.2m, 0.05m, 0.05m, 0.05m, 0.05m, 0.05m, 0.1m, 0.05m, 0.05m, 0.05m, 0.05m, 0.1m };

        public static readonly string[] FATE_CARD_TEXTS =
        {
            "Þú getur ekki einu sinni hlustað á útvarpsfréttirnar fyrir skruðningum og aukahljóðum í útvarpstækinu þínu. Þú gefst upp og kaupir alvöru-hljómflutningstæki frá Bang & Olufsen í Radíóbúðinni hf. Nú nýturðu hljómgæðanna þar sem þú situr í hægindastólnum með fjarstýringuna. Alvöru-hljómflutningstæki frá Bang & Olufsen kosta aðeins 1000.",
            "Þú kaupir ársmiða í leikhús, hann kostar 1500.",
            "Gamall félagi þinn frá braskárunum skýtur upp kollinum og hótar að leka ýmsu misjöfnu um þig í blöðin. Þú fellst á að borga þessum gamla félaga þínum sem situr þér á vinstri hönd, 200 og einnig að kaupa fyrir hann far til útlanda með Flugleiðum sem þú borgar 200 fyrir. Félaginn missir úr eitt kast vegna dvalar erlendis.",
            "Þú kaupir dekkjaganga á bílinn frá Sólningu hf. borgar 50.",
            "Fax... Að sjálfsögðu þarftu að eignast svoleiðis. Kaupir eitt frá Pósti og Síma á 200.",
            "Það var tími til kominn að þú færir í gott frí. Kaupir þér heimsreisu með Úrval-Útsýn borgar 400 og missir úr eitt kast vegna dvalar erlendis.",
            "Þú ferð til Akureyrar og skellir þér í Sjallann. Kvöldið er meiriháttar og þú skemmtir þér konunglega. Reikningurinn hljóðar uppá 350. Greiddur er út arður af Spariskírteinum.",
            "Sjóva-Almennar innheimta tryggingariðgjöld af bíla eigninni í spilinu. Hver maður á að borga 50 á bíl. Greiddur eru út arður af Spariskírteinum.",
            "Þú uppgötvar að allir stórir viðskiptajöfrar eiga bílasíma. Þú bregður þér í söludeild Pósts og síma og skellir þér á einn L.M. Ericson. Borgar 300 fyrir símann. Greiddur er út arður af Spariskírteinum.",
            "Dóttir þín hlustar ekki á neitt röfl! Hún innréttar herbergið sitt eftir nýjustu tísku. Þú borgar 400, en prísar þig sæla(n), fyrir hvað æðiskast hennar var í raun ódýrt.",
            "Þú kaupir hatt á makann hjá Hattari Rósa, þú færð vandaðann hatt á góðu verði. Þú borgar 50.",
            "Þú vinnur 1000 í Lotto 5/38. Greiddur eru út arður af Spariskírteinum.",
            "Þú ferð á reit Búnaðarbankans. Búnaðarbankinn, Traustur Banki.!",
            "Þú hafðir skrifað uppá skuldabréf hjá gömlum skólabróðir þínum. Hann stingur af til útlanda og þú þarft að borga bréfið með vöxtum og kostnaði alls 800. Greiddur er út arður af Spariskírteinum.",
            "Dragðu annað gluggaumslag og afhentu einhverjum spilafélaga þínum óséð.",
            "Þú ert í tak við nýja tíma. Ferð á reit íslandsbanka nú þegar.",
            "Það er búið að taka ávísunarheftið og fimm fingur af konunni þinni. Það var gert eftir að hún skrapp í CASA og mubleraði upp stofuna. Þú verður að greiða bankanum 2000 til að \"ballansera\" reikninginn.",
            "Þú ert í viðskiptaferð og týnist í Thailandi í eina viku. Þegar þú kemur skreyta persónulegir munir þínir gangstéttar hverfisins. Þú borgar 500 í fyrirframgreiðslu á íbúð sem þú tekur á leigu.",
            "Mikið óveður geysar um landið og allir vegir kolófærir. Jeppamenn komast því leiðar sinnar. Aðrir missa eitt kast.",
            "Þú sendir viðskiptavinum þínum erlendis gjafaáskrift af Iceland Revue. Borgar 50.",
            "Þú gerir vakt samning við Securitas og borgar 100 á hvert fyrirtæki sem þú átt hlut í. Greiddur er út arður af Spariskírteinum.",
            "Þú ert yfir þig ástfanginn og sendir elskunni þinni blóm á hverjum degi. Reikningurinn frá Blómaval hljóðar uppá 400, en þú greiðir hann með glöðu geði því blómasendingarnar virkuðu neflilega.",
            "Það hefur verið óskaplega mikið að gera undanfarið og þú lítið mátt vera að því að sinna makanum. Eins og gefur að skilja er hann verulega súr en gleymir öllu þegar þú birtist einn daginn með Loðfeld frá Eggerti Feldskera. Loðfeldurinn kostar þig aðeins 1000. Greiddur er út arður af Spariskírteinum.",
            "Þú tryggir ekki eftir á !! Tryggingariðgjöld Sjóvá-Almennra eru 100 á hvert fyrirtæki sem þú átt hlut í.",
            "Þú átt afmæli í dag til hamingju ! Greiddur er út arður af Spariskírteinum.",
            "Þú slærð upp lítilli veislu í fyrirtækinu. Þægilegasti kosturinn er tilbúin ostaveisla frá Osta og smjörsölunni. Þú borgar 500 fyrir veisluna.",
            "Þér greiðið 200 á hvert fyrirtæki sem þér eigið 75% eða meira í til Endurskoðanda þíns, vegna vinnu við endurskoðun ársreikninga fyrirtækjanna.",
            "Þú færð auglýsingastofu til að endurskipuleggja markaðsmál hjá fyrirtækjum þínum. Kostnaður við hvert fyrirtæki sem þú átt 50% eða meira í , er 500. Greiddur er út arður af Spariskírteinum.",
            "Nú er komið að því. Þú hefur ákveðið að kaupa þér íbúð. Eftir að hafa skoðað ótal íbúðir finnur þú loks eina sem þér líst á. Þú kaupir hana á 10.000. Þú mátt kjósa hvort þú borgar íbúðina núna eða nýtir þér gjaldfrest sem hægt er að fá. Kjósir þú að nýta þér gjaldfrestinn þá verður þú að borga íbúðina næst þegar þú kemur á verðbréfasölu ásamt vöxtum eins og þeir eru skráðir hverju sinni. Ef vextirnir eru 5% þá kostar íbúðin samtals 10.500. Ef vextirnir eru 10% þá kostar íbúðin samtals 12.000. Ef vextirnir eru 20% þá kostar íbúðin samtals 12.000.",
            "Örlætið þitt á sér engin takmörk. Þú styrkir íþróttafélagið þitt um 1000.",
            "Ef þú átt hlutabréf í Glitnir hf. rukkar þú félaga þína um afborganir af kaupleigusamningum, 500 hvern.",
            "Þú kaupir ræstingarvörur frá Sápugerðinni Frigg hf. fyrir 300. Greiddur er út arður af Spariskírteinum.",
            "Þig hefur alltaf langað til að eignast sumarbústað. Þú skellir þér á einn frá Bynor, Akureyri. Slotið með landi og öllu kostar aðeins 2000. Greiddur er út arður af Spariskírteinum.",
            "Þú festir bílinn í bakkgír og færist afturábak um þrjá reiti. Greiddur er út arður af Spariskírteinum.",
            "Sonur þinn er að stíga sín fyrstu skref í heimi viðskiptanna. Að sjálfsögðu er hann svolítill flækjufótur og kemur sér í slæma klípu. Þú verður að greiða 5000 í bankann vegna ábyrgða sem þú varst í fyrir hann. Greiddur er út arður af Spariskírteinum.",
            "Það er verið að ferma dóttur þína. Hún fékk að sjálfsögðu gras af seðlum og ákveður að fara í Verðbréfamarkað Íslandsbanka til að kynna sér helstu fjárfestingarmöguleikana. Þú slæst í för með henni svona rétt fyrir forvitnissakir. Ferð á næsta reit VÍB.",
            "Þú ert fædd(ur) undir ódrepandi óheillastjörnu. Makinn lendir í árekstri á besta bílnum. Ljósastaur stökk í veg fyrir bíl dótturinnar og gamalt..... ekur aftaná þig á umferðarljósum. Allir bílarnir eru gjörónýtir og þú missir þá alla. Þú tryggir ekki eftirá.!!",
            "Það vantar bensín á bílinn. Þú bregst skjótt við og ferð á reit Skeljungs til að fylla hann.",
            "Þú gerir samning við ræstingardeild Securitas um að ræsta fyrirtækin þín. Kostnaðurinn er 100 á hvert fyrirtæki sem þú átt 50% eða meira í.",
            "Miklar olíuverðhækkanir dynja yfir landsbyggðina. Hver leikmaður verður að greiða 800 sem skiptast jafnt á hlutabréfaeign í Skeljungi og Olíufélaginu.",
            "Þú borgar leigu fyrir ruslagáma frá Gámaþjónustunni hf. 50.",
            "Þú sendir starfsfólk í fyrirtækjum sem þú átt hlut í, á námskeið hjá Stjórnunnarfélaginu. Þú borgar 2 á hvern starfsmann.",
            "Þú ferð á sjúkrahús í andlitslyftingu, missir úr eitt kast. Greiddur er út arður af Spariskírteinum.",
            "Þú lætur ljósrita fundargögn í ljósritun Nóns hf. Þú borgar aðeins 100 fyrir viðvikið.",
            "Nú er illt í efni. Þú og maki þinn hafið ákveðið að skilja eftir að hafa tekið nokkrar lotur hjá prestinum. Sem betur fer hafðir þú vaðið fyrir neðan þig og gerðir kaupmála þegar þið kynntust fyrst. Samt sem áður verður þú að reiða fram andvirði 5% eigna þinna til að ganga frá málinu. (Eignir eru metnar á nafnverði) Maki þinn hljóp beint í fangið á bankastjóra spilabankans svo peningarnir renna í bankann.",
            "Þú mátt bjóða einum spilafélaga þínum í heimsókn í eitt af fyrirtækjum þínum að eigin vali, og hann verður að færa bíl sinn þangað. Greiddur er út arður af Spariskírteinum.",
            "Þú ferð á næsta uppboðsreit.",
            "Bíladellan eins og hún gerist verst hefur gripið þig, og það er komið að því að láta drauminn rætast. Þú kaupir  þér jeppa og thúnnar hann allan upp, ný dekk, upphækkun, flækjur, tvö dúsin af ljóskösturum, loran, skófla og vara viftureim gera bílinn ómótstæðilegann. Þú borgar bílinn og 800 í aukahluti frá Bílanaust. Ef engin jeppi er til þá helduru bara áfram að láta þig dreyma.",
            "Sonur þinn á afmæli. 18 ára er snáðinn orðinn og þú treystir honum ekki til að keyra einkabílinn þinn, svo þú kaupir handa honum ódýrasta bílinn sem völ er á Þú tryggir ekki eftirá !! Greiddur er út arður af Spariskírteinum.",
            "Ljósmyndadellan hefur gripið þig. Þú kaupir myndavél með öllum tilheyrandi búnaði frá Hans Petersen hf. Myndavélin kostar þig 500.",
            "Maki þinn fullyrti í morgun að varadekkin þrjú sem eru um þig miðjan væru ekkert mjög heillandi. Þér voru settir úrslitakostir. Annaðhvort gerðirðu eitthvað í málunum ellegar bæ.bæ. Þú verð í World Class, bryður prótein og borðar eingöngu allskyns heilsuvörur. Reikningurinn hljóðar samtals uppá 700 en þú ert hæst ánægð(ur) með árangurinn.",
            "Upplýsingar eru peningar. Þú kaupir stjörnukort um maka þinn. Þú borgar 50 fyrir kortið.",
            "Þú ferð að læra að fljúga hjá flugskóla Flugtaks. Harðákveðin(n( að vera reiðubúinn að fara hvert á land sem er á mikillar fyrirhafnar. Námskeiðið kostar aðeins 700. Greiddur er út arður af Spariskírteinum.",
            "Nú er komið að því að tölvuvæða fyrirtækin þín og þú velur heildarlausn frá Apple-umboðinu, nettengdar Apple Macintosh-tölvur, leysiprentara, skanna, geisladrif o.fl. Þú greiðir aðeins 500 á fyrirtæki sem þú átt 50% eða meira í.",
            "Þú færð Odda til að prenta fyrir þig tölvupappír og allan annan pappír sem þarf við rekstur skrifstofunnar. Borgar Odda 200. Oddi, þar sem prentun er list.",
            "Þú ferð á listaverka uppboð og kaupir málverk fyrir 1200.",
            "Svona stórathafnamenn eins og þú þurfa að vita hvað tímanum líður og auðvitað dugir ekkert annað en Rolexþ Þú greiðir aðeins 500 fyrir úrið.",
            "Nágranni þinn fékk sér gervihnattadisk fyrir hálfum mánuði og maki þinn hefur síðan þá legið uppi á háalofti með sjónauka og horft á þöglar myndir í gegnum gluggann. Þú unir þessu ekki lengur og pantar einn EchoStar gervihnattadisk frá Radíóbúðinni hf. Diskurinn er af vönduðustu gerð og kostar aðeins 200. Nú njótið þið hjónin ánægjulegra stunda í sófanum ykkar og veljið sjálf á hvað þið horfið.",
            "Félagi þinn sem situr þér á hægri hönd, býður í eitt af málverkum þímum. Þú fellst loks á að selja honum það á 700.",
            "Þú borgar ritfanga reikning frá Pennanum að upphæð 200"
        };

        public static readonly string[] GOVERNMENT_CARD_TEXTS =
        {
            "Þú gerist áskrifandi að Spariskírteinum Ríkissjóðs. Því færð þú fyrsta bréfið sem þú kaupir á sérkjörum, hálfvirði.",
            "Þú ferð með bílinn í skoðun hjá Bifreiðaskoðun Íslands, borgar 100 á hvern bíl.",
            "Þú verður að borga sekt fyrir ofhraðann akstur, að upphæð 300. Greiddur er út arður af Spariskírteinum.",
            "Ríkistjórnin leggur á aukaskatt á hagnað. Þú verður að greiða 50% af öllu lausafé þínu.",
            "Upp kemst að þú hefur verið með ólöglegt sjónvarpstæki í 3 ár, án þess að greiða afnotagjöldin. Greiddu strax 100 til að forðast frekari innheimtuaðgerðir.",
            "Gjalddagi bifreiðagjalda er kominn, allir verða að greiða 50 á hvern bíl sem þeir kunna að eiga.",
            "Upp komst um svik í tollskýrslu, þú greiðir 400 í sekt.",
            "Þér er boðið í forsetaveislu. Ekki er um annað að ræð aen að koma við hjá Sævari Karli og Sonum. Einfaldi smekkurinn kostar þig aðeins 400.",
            "Þú borgar 1500 í kosningasjóð flokks þíns.",
            "Þú ert tekinn á ofsahraða ölvaður við stýrið á leiðinni úr \"kokteil\" boði. Þú ert sviptur ökuleyfinu á staðnum og verður að borga risasekt 300 og sitja hjá eina umferð.",
            "Gamall skólabróðir þinn fær sæti í Byggðarstofnun. Hann tryggir þér lánsloforð að upphæð 5000. Þú verður að borga lánið næst þegar þú kemur á verðbréfasölu.",
            "Þú færð greidd laun fyrir nefndarstörf og fyrir störf í bankaráði, 300.",
            "Skipt er um ríkisstjórn og hún grípur til harkalegra aðgerða í efnahagsmálum, sem virka aldrei þessu vant. Þú færð greiddan 10% arð af allri hlutafjáreign þinni. (Nafnverð)",
            "Þinn flokkur tekur við stjórnartaumunum. Þar sem þú ert virkur flokksfélagi getur þetta komið sér vel fyrir þig.!",
            "Besta fyrirtækið þitt er innsiglað vegna vangoldins virðisaukaskatts. Fyrirtækið er innsiglað í eina umferð og þú getur því ekki innheimt arð ef einhver lendir á reit þess. Eftir að umferðinni er lokið verður þú að greiða virðisaukaskatt og álögur samtals 1500 til að opna fyrirtækið. Spilafélagar þínir leggja mat á hvaða fyrirtæki er best.",
            "Þú ferð í lax með forsætiráðherra, hann hrífst ákaflega að nýja jeppanum þínu og kaupir hann af þér á 400.",
            "Stjórnarkreppa ríkir í landinu, eins og í öllum öðrum \"Bananalýðveldum\" skapast mikil óvissa á verðbréfamörkuðum og verslun er hætt í eina umferð.",
            "Tvær bifreiðar þínar eru boðnar upp vegna vangoldinna bifreiðagjalda. Tekjur af uppboðinu fara í kröfu Ríkissjóðs.",
            "Ný ríkisstjórn tekur við völdum. Þónokkrar breytingar verða á efnahagslífinu og skipt er um gengistöflu. Þú bregst snöggt við og bregður þér á næst reit Verðbréfamarkaðst Íslandsbanka.",
            "Til hamingju þú færð fálkaorðuna, en það tekur engin eftir því. Greiddur er út arður af Spariskírteinum.",
            "Ef þú átt fyrirtæki með samtals 2000 starfsmenn(100% eign) þá styrkir ríkið þig með 1000 vegna þáttöku þinnar í eflingu atvinnulífsins.",
            "Nú er komið að innheimtu útsvars og Aðstöðugjalda. Þér verðið að greiða 200 á hvert fyrirtæki sem þú átt 50% eða meira í.",
            "Borgar kirkjugarðsgjald að upphæð 50.",
            "Stóreignaskattur !! Þar lendir þú í því, \"Skattman\" leggur á þig stóreignaskatt. Ef þú átt 15 fyrirtæki í hreinni eign (100%) þá verður þú að borga 20000 í stóreignarskatt.",
            "Skattframtal þitt síðustu 3ja ára er tekið til endurskoðunar. Að sjálfsögðu er ekki allt með felldu og þú verður að greiða viðurlögur vegna smávægilegs undanskots, en aðeins 600 vegna þess að þú hefur læknisvottorð uppá brenglaða siðferðisvitund.",
            "Ef þú átt meira en 40.000 í peningum þá tekur ríkið allt sem framyfir er í skatt.",
            "Gamall frændi þinn deyr, sorg þín er mikil en linast þó er í ljós kemur að hann hefur verið áskrifandi að Spariskírteinum Ríkissjóðs, og þú hefur erft eitt spariskírteini á 5000.",
            "Þú borgar stöðumælasekt 50. Greiddur er út arður af Spariskírteinum.",
            "Þú hefur fengið óeðlilega fyrirgreiðslu úr einum af hinum opinberu sjóðum. Dagblöðin fletta ofan af málinu og fyrirtæki þín verða fyrir þónokkrum skakkaföllum. Þú missir úr eitt kast meða málið fyrnist."
        };

        public static Company[] COMPANIES;
        public static PrivateCompany[] PRIVATE_COMPANIES;
        public static Dictionary<CompanyNamesEnum, Company> CompanyDictionary;
        public static Dictionary<CompanyNamesEnum, Company> PrivateCompanyDictionary;
        public static Car[] CARS;
        public static readonly Image[] COMPANY_IMAGE;
        public static readonly Image[] COMPANY_SMALL_IMAGE;
        public static readonly Image[] PRIVATE_COMPANY_IMAGE;
        public static readonly Image[] PRIVATE_COMPANY_SMALL_IMAGE;

        static Constants()
        {
            COMPANY_IMAGE = new Image[30];
            COMPANY_SMALL_IMAGE = new Image[30];
            PRIVATE_COMPANY_IMAGE = new Image[30];
            PRIVATE_COMPANY_SMALL_IMAGE = new Image[30];
            CompanyDictionary = new Dictionary<CompanyNamesEnum, Company>();
            PrivateCompanyDictionary = new Dictionary<CompanyNamesEnum, Company>();
            int id = 0;
            COMPANIES = new Company[30];
            COMPANIES[id] = new Company(id, "Ellingsen", 100, BusinessGroup.Green, Properties.Resources.logoEllingsen, Properties.Resources.logoEllingsen_small, Properties.Resources.logoEllingsen_tiny);
            CompanyDictionary.Add(CompanyNamesEnum.Ellingsen, COMPANIES[id++]);
            COMPANIES[id] = new Company(id, "Papco", 40, BusinessGroup.Green, Properties.Resources.logoPapco, Properties.Resources.logoPapco_small, Properties.Resources.logoPapco_tiny);
            CompanyDictionary.Add(CompanyNamesEnum.Papco, COMPANIES[id++]);
            COMPANIES[id] = new Company(id, "Tré-x", 160, BusinessGroup.Green, Properties.Resources.logoTrex, Properties.Resources.logoTrex_small, Properties.Resources.logoTrex_tiny);
            CompanyDictionary.Add(CompanyNamesEnum.TreX, COMPANIES[id++]);
            COMPANIES[id] = new Company(id, "Móna", 80, BusinessGroup.Green, Properties.Resources.logoMona, Properties.Resources.logoMona_small, Properties.Resources.logoMona_tiny);
            CompanyDictionary.Add(CompanyNamesEnum.Mona, COMPANIES[id++]);
            COMPANIES[id] = new Company(id, "HF. Ofnasmiðjan", 120, BusinessGroup.Green, Properties.Resources.logoOfnasmidjan, Properties.Resources.logoOfnasmidjan_small, Properties.Resources.logoOfnasmidjan_tiny);
            CompanyDictionary.Add(CompanyNamesEnum.Ofnasmidjan, COMPANIES[id++]);

            COMPANIES[id] = new Company(id, "Úrval-Útsýn", 240, BusinessGroup.Orange, Properties.Resources.logoUrvalUtsyn, Properties.Resources.logoUrvalUtsyn_small, Properties.Resources.logoUrvalUtsyn_tiny);
            CompanyDictionary.Add(CompanyNamesEnum.UrvalUtsyn, COMPANIES[id++]);
            COMPANIES[id] = new Company(id, "MAX", 160, BusinessGroup.Orange, Properties.Resources.logoMax, Properties.Resources.logoMax_small, Properties.Resources.logoMax_tiny);
            CompanyDictionary.Add(CompanyNamesEnum.MAX, COMPANIES[id++]);
            COMPANIES[id] = new Company(id, "Íspan", 160, BusinessGroup.Orange, Properties.Resources.logoIspan, Properties.Resources.logoIspan_small, Properties.Resources.logoIspan_tiny);
            CompanyDictionary.Add(CompanyNamesEnum.Ispan, COMPANIES[id++]);
            COMPANIES[id] = new Company(id, "Strikið", 160, BusinessGroup.Orange, Properties.Resources.logoStrikid, Properties.Resources.logoStrikid_small, Properties.Resources.logoStrikid_tiny);
            CompanyDictionary.Add(CompanyNamesEnum.Strikid, COMPANIES[id++]);
            COMPANIES[id] = new Company(id, "Frón", 160, BusinessGroup.Orange, Properties.Resources.logoFron, Properties.Resources.logoFron_small, Properties.Resources.logoFron_tiny);
            CompanyDictionary.Add(CompanyNamesEnum.Fron, COMPANIES[id++]);

            COMPANIES[id] = new Company(id, "Málning hf", 200, BusinessGroup.Pink, Properties.Resources.logoMalning, Properties.Resources.logoMalning_small, Properties.Resources.logoMalning_tiny);
            CompanyDictionary.Add(CompanyNamesEnum.Malning, COMPANIES[id++]);
            COMPANIES[id] = new Company(id, "Bílanaust", 160, BusinessGroup.Pink, Properties.Resources.logoBilanaust, Properties.Resources.logoBilanaust_small, Properties.Resources.logoBilanaust_tiny);
            CompanyDictionary.Add(CompanyNamesEnum.Bilanaust, COMPANIES[id++]);
            COMPANIES[id] = new Company(id, "Sæplast", 120, BusinessGroup.Pink, Properties.Resources.logoSaeplast, Properties.Resources.logoSaeplast_small, Properties.Resources.logoSaeplast_tiny);
            CompanyDictionary.Add(CompanyNamesEnum.Saeplast, COMPANIES[id++]);
            COMPANIES[id] = new Company(id, "Tollvörugeymslan", 120, BusinessGroup.Pink, Properties.Resources.logoTollvorugeymslan, Properties.Resources.logoTollvorugeymslan_small, Properties.Resources.logoTollvorugeymslan_tiny);
            CompanyDictionary.Add(CompanyNamesEnum.Tollvorugeymslan, COMPANIES[id++]);
            COMPANIES[id] = new Company(id, "Radíóbúðin", 160, BusinessGroup.Pink, Properties.Resources.logoRadiobudin, Properties.Resources.logoRadiobudin_small, Properties.Resources.logoRadiobudin_tiny);
            CompanyDictionary.Add(CompanyNamesEnum.Radiobudin, COMPANIES[id++]);

            COMPANIES[id] = new Company(id, "ÓJ&K", 200, BusinessGroup.Tan, Properties.Resources.logoOjk, Properties.Resources.logoOjk_small, Properties.Resources.logoOjk_tiny);
            CompanyDictionary.Add(CompanyNamesEnum.OJK, COMPANIES[id++]);
            COMPANIES[id] = new Company(id, "Kodak", 200, BusinessGroup.Tan, Properties.Resources.logoKodak, Properties.Resources.logoKodak_small, Properties.Resources.logoKodak_tiny);
            CompanyDictionary.Add(CompanyNamesEnum.Kodak, COMPANIES[id++]);
            COMPANIES[id] = new Company(id, "Jöfur hf", 200, BusinessGroup.Tan, Properties.Resources.logoJofur, Properties.Resources.logoJofur_small, Properties.Resources.logoJofur_tiny);
            CompanyDictionary.Add(CompanyNamesEnum.Jofur, COMPANIES[id++]);
            COMPANIES[id] = new Company(id, "Héðinn hf", 200, BusinessGroup.Tan, Properties.Resources.logoHedinn, Properties.Resources.logoHedinn_small, Properties.Resources.logoHedinn_tiny);
            CompanyDictionary.Add(CompanyNamesEnum.Hedinn, COMPANIES[id++]);
            COMPANIES[id] = new Company(id, "Oddi", 200, BusinessGroup.Tan, Properties.Resources.logoOddi, Properties.Resources.logoOddi_small, Properties.Resources.logoOddi_tiny);
            CompanyDictionary.Add(CompanyNamesEnum.Oddi, COMPANIES[id++]);

            COMPANIES[id] = new Company(id, "Glitnir hf", 200, BusinessGroup.Purple, Properties.Resources.logoGlitnir, Properties.Resources.logoGlitnir_small, Properties.Resources.logoGlitnir_tiny);
            CompanyDictionary.Add(CompanyNamesEnum.Glitnir, COMPANIES[id++]);
            COMPANIES[id] = new Company(id, "Sjóvá-Almennar", 480, BusinessGroup.Purple, Properties.Resources.logoSjova, Properties.Resources.logoSjova_small, Properties.Resources.logoSjova_tiny);
            CompanyDictionary.Add(CompanyNamesEnum.SjovaAlmennar, COMPANIES[id++]);
            COMPANIES[id] = new Company(id, "Grandi hf", 1320, BusinessGroup.Purple, Properties.Resources.logoGrandi, Properties.Resources.logoGrandi_small, Properties.Resources.logoGrandi_tiny);
            CompanyDictionary.Add(CompanyNamesEnum.Grandi, COMPANIES[id++]);
            COMPANIES[id] = new Company(id, "Skeljungur hf", 1040, BusinessGroup.Purple, Properties.Resources.logoSkeljungur, Properties.Resources.logoSkeljungur_small, Properties.Resources.logoSkeljungur_tiny);
            CompanyDictionary.Add(CompanyNamesEnum.Skeljungur, COMPANIES[id++]);
            COMPANIES[id] = new Company(id, "Vífilfell hf", 520, BusinessGroup.Purple, Properties.Resources.logoVifilfell, Properties.Resources.logoVifilfell_small, Properties.Resources.logoVifilfell_tiny);
            CompanyDictionary.Add(CompanyNamesEnum.Vifilfell, COMPANIES[id++]);

            COMPANIES[id] = new Company(id, "ESSO/Olíufélagið", 1240, BusinessGroup.DarkGreen, Properties.Resources.logoOliufelagid, Properties.Resources.logoOliufelagid_small, Properties.Resources.logoOliufelagid_tiny);
            CompanyDictionary.Add(CompanyNamesEnum.Esso, COMPANIES[id++]);
            COMPANIES[id] = new Company(id, "Eimskip", 2800, BusinessGroup.DarkGreen, Properties.Resources.logoEimskip, Properties.Resources.logoEimskip_small, Properties.Resources.logoEimskip_tiny);
            CompanyDictionary.Add(CompanyNamesEnum.Eimskip, COMPANIES[id++]);
            COMPANIES[id] = new Company(id, "Íslandsbanki", 3600, BusinessGroup.DarkGreen, Properties.Resources.logoIslandsbanki, Properties.Resources.logoIslandsbanki_small, Properties.Resources.logoIslandsbanki_tiny);
            CompanyDictionary.Add(CompanyNamesEnum.Islandsbanki, COMPANIES[id++]);
            COMPANIES[id] = new Company(id, "Flugleiðir", 5000, BusinessGroup.DarkGreen, Properties.Resources.logoFlugleidir, Properties.Resources.logoFlugleidir_small, Properties.Resources.logoFlugleidir_tiny);
            CompanyDictionary.Add(CompanyNamesEnum.Flugleidir, COMPANIES[id++]);
            COMPANIES[id] = new Company(id, "Búnaðarbankinn", 2200, BusinessGroup.DarkGreen, Properties.Resources.logoBunadarbankinn, Properties.Resources.logoBunadarbankinn_small, Properties.Resources.logoBunadarbankinn_tiny);
            CompanyDictionary.Add(CompanyNamesEnum.Bunadarbankinn, COMPANIES[id++]);

            id = 0;
            PRIVATE_COMPANIES = new PrivateCompany[6];
            PRIVATE_COMPANIES[id] = new PrivateCompany(id, "Crown Chicken", 10, 800, new int[] { 500, 1000, 2000 }, Properties.Resources.logoCrownChicken, Properties.Resources.logoCrownChicken_small, Properties.Resources.logoCrownChicken_tiny);
            PrivateCompanyDictionary.Add(CompanyNamesEnum.CrownChicken, COMPANIES[id++]);
            PRIVATE_COMPANIES[id] = new PrivateCompany(id, "Bíóborgin", 10, 1500, new int[] { 1000, 2000, 2000, 4000 }, Properties.Resources.logoBioborgin, Properties.Resources.logoBioborgin_small, Properties.Resources.logoBioborgin_tiny);
            PrivateCompanyDictionary.Add(CompanyNamesEnum.Bioborgin, COMPANIES[id++]);
            PRIVATE_COMPANIES[id] = new PrivateCompany(id, "Bíóhöllin", 10, 1500, new int[] { 1000, 2000, 2000, 4000 }, Properties.Resources.logoBiohollin, Properties.Resources.logoBiohollin_small, Properties.Resources.logoBiohollin_tiny);
            PrivateCompanyDictionary.Add(CompanyNamesEnum.Biohollin, COMPANIES[id++]);
            PRIVATE_COMPANIES[id] = new PrivateCompany(id, "Höldur sf.", 10, 3000, new int[] { 2000, 4000, 8000 }, Properties.Resources.logoHoldur, Properties.Resources.logoHoldur_small, Properties.Resources.logoHoldur_tiny);
            PrivateCompanyDictionary.Add(CompanyNamesEnum.Holdur, COMPANIES[id++]);
            PRIVATE_COMPANIES[id] = new PrivateCompany(id, "Penninn", 10, 2500, new int[] { 1700, 3400, 6800 }, Properties.Resources.logoPenninn, Properties.Resources.logoPenninn_small, Properties.Resources.logoPenninn_tiny);
            PrivateCompanyDictionary.Add(CompanyNamesEnum.Penninn, COMPANIES[id++]);
            PRIVATE_COMPANIES[id] = new PrivateCompany(id, "Sveinn Bakari", 10, 1500, new int[] { 1000, 1500, 4000 }, Properties.Resources.logoSveinnBakari, Properties.Resources.logoSveinnBakari_small, Properties.Resources.logoSveinnBakari_tiny);
            PrivateCompanyDictionary.Add(CompanyNamesEnum.SveinnBakari, COMPANIES[id++]);

            id = 0;
            CARS = new Car[15];
            CARS[id++] = new Car("Skoda favorit 136 LS", 300, Enums.CarCategory.Regular, "5. gíra", 840, 7, 1298, 63);
            CARS[id++] = new Car("Peugeot 309 XL", 500, Enums.CarCategory.Regular, "5. gíra", 800, 7.4m, 1360, 80);
            CARS[id++] = new Car("Peugeot 205 XR", 500, Enums.CarCategory.Regular, "5. gíra", 750, 6.4m, 1360, 70);
            CARS[id++] = new Car("Peugeot 205 XS", 500, Enums.CarCategory.Regular, "5. gíra", 775, 7, 1360, 85);
            CARS[id++] = new Car("Peugeot 309 GTI", 500, Enums.CarCategory.Regular, "5. gíra", 950, 8.2m, 1905, 122);
            CARS[id++] = new Car("Peugeot 205 GTI 1.6", 500, Enums.CarCategory.Regular, "5. gíra", 825, 7.5m, 1580, 115);
            CARS[id++] = new Car("Chrysler Saratoga 3.0 i V-6", 1000, Enums.CarCategory.Regular, "Sjálfskiptur", 1400, 10.6m, 3000, 141);
            CARS[id++] = new Car("Peugeot Mi 16", 1000, Enums.CarCategory.Regular, "5. gíra", 1025, 9, 1905, 125);
            CARS[id++] = new Car("Peugeot 405 SRI", 1000, Enums.CarCategory.Regular, "Sjálfskiptur", 1025, 9, 1905, 125);
            CARS[id++] = new Car("Peugeot 405 GR", 1000, Enums.CarCategory.Regular, "Sjálfskiptur", 1000, 8, 1905, 110);
            CARS[id++] = new Car("Peugeot 605 SV 3.0 i", 1500, Enums.CarCategory.Regular, "Sjálfskiptur", 1415, 12.5m, 3975, 170);
            CARS[id++] = new Car("Jeep Wrangler 4x4", 1500, Enums.CarCategory.SUV, "Sjálfskiptur / 5. gíra", 1455, 15, 3960, 182);
            CARS[id++] = new Car("Cherokee Limited ABS 4x4", 1500, Enums.CarCategory.SUV, "Sjálfskiptur", 1570, 14, 3960, 190);
            CARS[id++] = new Car("Cherokee Laredo Euro 4x4", 1500, Enums.CarCategory.SUV, "Sjálfskiptur", 1535, 14, 3960, 190);
            CARS[id++] = new Car("Voyager AWD 3.3 V6 4x4 7. Manna", 1500, Enums.CarCategory.SUV, "Sjálfskiptur", 1745, 12.5m, 3301, 148);
        }

        public enum CompanyNamesEnum
        {
            Ellingsen = 0,
            Papco = 1,
            TreX = 2,
            Mona = 3,
            Ofnasmidjan = 4,
            UrvalUtsyn = 5,
            MAX = 6,
            Ispan = 7,
            Strikid = 8,
            Fron = 9,
            Malning = 10,
            Bilanaust = 11,
            Saeplast = 12,
            Tollvorugeymslan = 13,
            Radiobudin = 14,
            OJK = 15,
            Kodak = 16,
            Jofur = 17,
            Hedinn = 18,
            Oddi = 19,
            Glitnir = 20,
            SjovaAlmennar = 21,
            Grandi = 22,
            Skeljungur = 23,
            Vifilfell = 24,
            Esso = 25,
            Eimskip = 26,
            Islandsbanki = 27,
            Flugleidir = 28,
            Bunadarbankinn = 29,

            CrownChicken = 0,
            Bioborgin = 1,
            Biohollin = 2,
            Holdur = 3,
            Penninn = 4,
            SveinnBakari = 5
        }
    }
}
