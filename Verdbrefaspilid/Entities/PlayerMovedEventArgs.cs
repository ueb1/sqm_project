﻿using Game.Entities;
using Game.Entities.Cells;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Entities
{
    public class PlayerMovedEventArgs : EventArgs
    {
        public Player Player { get; set; }
        public Cell NewCell { get; set; }

        public PlayerMovedEventArgs(Player player, Cell newCell)
        {
            Player = player;
            NewCell = newCell;
        }
    }
}
