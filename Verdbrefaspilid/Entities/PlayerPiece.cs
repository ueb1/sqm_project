﻿using Game.Entities.Cells;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Entities
{
    public class PlayerPiece
    {
        public Player Player { get; set; }
        public int CellNumber { get; set; }
        public Image Icon { get; set; }

        public void Move(int destinationCellNr)
        {
            CellNumber = destinationCellNr;
        }
    }
}
