﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Entities.Enums
{
    public enum EventTypeEnum
    {
        PlayerLosesMoneyToBank,
        PlayerGetsMoneyFromBank,
        PlayerGivesMoneyToPlayer
    }
}
