﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Entities.Enums
{
    public enum CarCategory
    {
        Regular = 0,
        SUV = 1
    }
}
