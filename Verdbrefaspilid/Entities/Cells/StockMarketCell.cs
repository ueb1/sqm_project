﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Entities.Cells
{
    class StockMarketCell : Cell
    {
        #region Private members

        #endregion

        #region Attributes

        #endregion

        public StockMarketCell(int x, int y, int width, int height, bool changeExchangeRate) : base(x, y, width, height, changeExchangeRate)
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // StockMarketCell
            // 
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.StockMarketCell_Paint);
            this.ResumeLayout(false);

        }

        private void StockMarketCell_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            e.Graphics.DrawImage(Properties.Resources.stockMarketCell, 0, 0, Width, Height);
            e.Graphics.FillRectangle(Brushes.Blue, 0, 0, 8, 8);
            e.Graphics.FillRectangle(Brushes.Blue, Width - 8, 0, 8, 8);
            e.Graphics.FillRectangle(Brushes.Blue, 0, Height - 8, 8, 8);
            e.Graphics.FillRectangle(Brushes.Blue, Width - 8, Height - 8, 8, 8);
        }
    }
}
