﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Entities.Cells
{
    class FateCell : Cell
    {
        #region Private members

        #endregion

        #region Attributes

        #endregion

        public FateCell(int x, int y, int width, int height, bool changeExchangeRate) : base(x, y, width, height, changeExchangeRate)
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // FateCell
            // 
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.FateCell_Paint);
            this.ResumeLayout(false);

        }

        private void FateCell_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            e.Graphics.DrawImage(Properties.Resources.FateCell, 0, 0, Width, Height);
        }
    }
}
