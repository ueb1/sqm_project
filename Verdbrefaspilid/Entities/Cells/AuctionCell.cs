﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Entities.Cells
{
    class AuctionCell : Cell
    {
        #region Private members

        #endregion

        #region Attributes

        #endregion

        public AuctionCell(int x, int y, int width, int height, bool changeExchangeRate) : base(x, y, width, height, changeExchangeRate)
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // AuctionCell
            // 
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.AuctionCell_Paint);
            this.ResumeLayout(false);

        }

        private void AuctionCell_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            e.Graphics.DrawImage(Properties.Resources.AuctionCell, 0, 0, Width, Height);
        }
    }
}
