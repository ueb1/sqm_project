﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Entities
{
    public class Die
    {
        private static Random rand;
        private int value;

        public int Value {
            get { return value; }
        }

        static Die()
        {
            rand = new Random();
        }

        public int Roll()
        {
            int roll = rand.Next(1, 7);
            value = roll;
            return roll;
        }
    }
}
