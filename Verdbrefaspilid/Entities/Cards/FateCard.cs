﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Entities.Cards
{
    public class FateCard : Card
    {
        private static int NextId = 0;

        #region Private members
        private string text;
        private int cardId;
        private string cardName;
        #endregion

        #region Attributes
        public string Text
        {
            get
            {
                return text;
            }
        }

        public int CardId
        {
            get
            {
                return cardId;
            }
        }

        public string CardName
        {
            get
            {
                return cardName;
            }
        }
        #endregion

        public FateCard(string text) : base("Gluggaspjald")
        {
            this.text = text;
            this.cardId = NextId++;
        }

        public FateCard(string name, string text) : base("Gluggaspjald")
        {
            this.text = text;
            this.cardName = name;
            this.cardId = NextId++;
        }
    }
}
