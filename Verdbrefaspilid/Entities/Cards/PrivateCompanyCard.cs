﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Entities.Cards
{
    public class PrivateCompanyCard : Card
    {
        #region Private members
        private PrivateCompany company;
        #endregion

        #region Attributes
        public PrivateCompany Company
        {
            get
            {
                return company;
            }
        }
        public int Value
        {
            get
            {
                return company.Value;
            }
        }
        #endregion

        public PrivateCompanyCard(PrivateCompany company) : base(company.CompanyName)
        {
            this.company = company;
        }
    }
}
