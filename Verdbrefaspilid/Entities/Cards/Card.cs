﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Entities.Cards
{
    public abstract class Card
    {
        private string title;
        public string Title
        {
            get
            {
                return title;
            }
        }

        public Card(string title)
        {
            this.title = title;
        }
    }
}
