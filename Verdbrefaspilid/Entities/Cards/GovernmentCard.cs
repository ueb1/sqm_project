﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Entities.Cards
{
    public class GovernmentCard : Card
    {
        #region Private members
        private string text;
        #endregion

        #region Attributes
        public string Text
        {
            get
            {
                return text;
            }
        }

        #endregion

        public GovernmentCard(string text) : base("Hið opinbera")
        {
            this.text = text;
        }
    }
}
