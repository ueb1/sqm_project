﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Entities.Cards
{
    public class BankCard : Card
    {
        #region Private members
        private string name;
        private int amount;
        private bool isDeposit;
        #endregion

        #region Attributes
        public string Name
        {
            get
            {
                return name;
            }
        }

        public int Amount
        {
            get
            {
                return amount;
            }
        }

        public bool IsDeposit
        {
            get
            {
                return isDeposit;
            }
        }
        #endregion

        public BankCard(int amount, bool isDeposit) : base(isDeposit ? "Gullbók" : "Bankalán")
        {
            this.amount = amount;
            this.isDeposit = isDeposit;
            if (isDeposit)
                this.name = "Gullbók";
            else
                this.name = "Bankalán";
        }
    }
}
