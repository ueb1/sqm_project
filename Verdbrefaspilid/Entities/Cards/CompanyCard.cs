﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Entities.Cards
{
    public class CompanyCard : Card
    {
        #region Private members
        private Company company;
        #endregion

        #region Attributes
        public Company Company
        {
            get
            {
                return company;
            }
        }

        public int ShareValue
        {
            get
            {
                return Constants.COMPANY_SHARE_COST[(int)company.BusinessGroup] / Constants.NR_COMPANY_SHARES;
            }
        }
        #endregion

        public CompanyCard(Company company) : base (company.CompanyName)
        {
            this.company = company;
        }

        public CompanyCard(Company company, Color cardColor) : base(company.CompanyName)
        {
            this.company = company;
        }

        public CompanyCard(Company company, Color cardColor, Image smallImage, Image largeImage) : base(company.CompanyName)
        {
            this.company = company;
        }
    }
}
