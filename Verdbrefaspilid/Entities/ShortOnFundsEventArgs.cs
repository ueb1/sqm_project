﻿using Game.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Entities
{
    public class ShortOnFundsEventArgs : EventArgs
    {
        public int DueAmount { get; set; }
        public Player Debtee { get; set; }
        public bool DebtToBank { get; set; }

        public ShortOnFundsEventArgs(int dueAmount, bool debtToBank)
        {
            DueAmount = dueAmount;
            DebtToBank = debtToBank;
        }

        public ShortOnFundsEventArgs(int dueAmount, Player debtee)
        {
            DueAmount = dueAmount;
            Debtee = debtee;
        }
    }
}
