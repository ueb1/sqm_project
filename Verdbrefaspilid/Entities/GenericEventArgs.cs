﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Entities
{
    public class GenericEventArgs<T> : EventArgs
    {
        public T Value { get; set; }

        public GenericEventArgs(T value)
        {
            Value = value;
        }
    }
}
