﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Game.Dialogs
{
    public partial class GenericDialog : Form
    {

        public GenericDialog(string text)
        {
            InitializeComponent();
            lblText.Text = text;
            btnMiddle.Visible = true;
            btnMiddle.DialogResult = DialogResult.OK;
        }

        public GenericDialog(string text, MessageBoxButtons buttons) : this(text)
        {
            switch (buttons)
            {
                case MessageBoxButtons.AbortRetryIgnore:
                    btnLeft.Visible = true;
                    btnMiddle.Visible = true;
                    btnRight.Visible = true;
                    btnLeft.Text = "Hætta við";
                    btnLeft.DialogResult = DialogResult.Abort;
                    btnMiddle.Text = "Reyna aftur";
                    btnMiddle.DialogResult = DialogResult.Retry;
                    btnRight.Text = "Hunsa";
                    btnRight.DialogResult = DialogResult.Ignore;
                    break;
                case MessageBoxButtons.OK:
                    btnLeft.Visible = false;
                    btnMiddle.Visible = true;
                    btnRight.Visible = false;
                    btnMiddle.Text = "Í lagi";
                    btnMiddle.DialogResult = DialogResult.OK;
                    break;
                case MessageBoxButtons.OKCancel:
                    btnMiddle.Visible = false;
                    btnLeft.Visible = true;
                    btnRight.Visible = true;
                    btnLeft.Text = "Í lagi";
                    btnLeft.DialogResult = DialogResult.OK;
                    btnRight.Text = "Hætta við";
                    btnRight.DialogResult = DialogResult.Cancel;
                    break;
                case MessageBoxButtons.RetryCancel:
                    btnLeft.Visible = true;
                    btnMiddle.Visible = false;
                    btnRight.Visible = true;
                    btnLeft.Text = "Reyna aftur";
                    btnLeft.DialogResult = DialogResult.Retry;
                    btnRight.Text = "Hætta við";
                    btnRight.DialogResult = DialogResult.Cancel;
                    break;
                case MessageBoxButtons.YesNo:
                    btnLeft.Visible = true;
                    btnMiddle.Visible = false;
                    btnRight.Visible = true;
                    btnLeft.Text = "Já";
                    btnLeft.DialogResult = DialogResult.Yes;
                    btnRight.Text = "Nei";
                    btnRight.DialogResult = DialogResult.No;
                    break;
                case MessageBoxButtons.YesNoCancel:
                    btnLeft.Visible = true;
                    btnMiddle.Visible = true;
                    btnRight.Visible = true;
                    btnLeft.Text = "Já";
                    btnLeft.DialogResult = DialogResult.Yes;
                    btnMiddle.Text = "Nei";
                    btnMiddle.DialogResult = DialogResult.No;
                    btnRight.Text = "Hætta við";
                    btnRight.DialogResult = DialogResult.Cancel;
                    break;
            }
        }

        public static DialogResult ShowDialog(string text, MessageBoxButtons buttons)
        {
            // using construct ensures the resources are freed when form is closed
            using (var form = new GenericDialog(text, buttons))
            {
                return form.ShowDialog();
            }
        }

        public static DialogResult ShowDialog(string text)
        {
            // using construct ensures the resources are freed when form is closed
            using (var form = new GenericDialog(text))
            {
                return form.ShowDialog();
            }
        }

        private void GenericDialog_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawRectangle(Pens.Black, 0, 0, this.Width - 1, this.Height - 1);
        }
    }
}
