﻿using Game.Dialogs;
using Game.Entities;
using Game.Entities.Cards;
using Game.Entities.Cells;
using Game.Entities.Enums;
using Game.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Game
{
    public partial class MainForm : Form
    {
        private Player[] players;
        private int currentPlayer;
        private Bank bank;
        private List<FateCard> fateCardDeck;
        private List<GovernmentCard> governmentCardDeck;
        private CompanyCard[,] companyCards;
        //Dictionary<int, PrivateCompany> privateCompanies;
        private Cell[] cells;
        private StartCell startCell;
        private int nrOfPlayers;
        private bool continueDebugging;
        private Random rand;
        private Cell currentCell;
        private bool debugMode;

        private BoardView boardView;
        private WalletView walletView;
        private Control currentView;

        public MainForm()
        {
            InitializeComponent();
            continueDebugging = true;
            debugMode = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("DebugMode"));
            rand = new Random();

            boardView = new BoardView(debugMode);
            boardView.MoveFinished += BoardView_MoveFinished;
            boardView.DebugActivateFateCard += BoardView_DebugActivateFateCard;
            boardView.DebugActivateGovernmentCard += BoardView_DebugActivateGovernmentCard;
            boardView.btnFinishTurn.Click += BtnFinishTurn_Click;
            boardView.btnCellEvent.Click += BtnCellEvent_Click;
            walletView = new WalletView();
            walletView.button1.Click += Button1_Click;
        }

        private void BoardView_DebugActivateGovernmentCard(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void BoardView_DebugActivateFateCard(object sender, int cardId)
        {
            FateCard card = fateCardDeck.FirstOrDefault(x => x.CardId == cardId);
            if (card != null)
            {
                activateFateCard(card);
                boardView.RefreshAssetsBoard(players[currentPlayer].Wallet);
            }
        }

        private void BtnCellEvent_Click(object sender, EventArgs e)
        {
            ShowCellEvent(currentCell, players[currentPlayer]);
        }

        private void BtnFinishTurn_Click(object sender, EventArgs e)
        {
            currentPlayer = (currentPlayer + 1) % nrOfPlayers;
            while (players[currentPlayer].MissesNextTurn == true)
            {
                players[currentPlayer].MissesNextTurn = false;
                currentPlayer = (currentPlayer + 1) % nrOfPlayers;
            }

            boardView.SetNextPlayer(currentPlayer);
            boardView.btnFinishTurn.Visible = false;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            ChangeView(boardView);
        }

        private void BoardView_MoveFinished(object sender, EventArgs e)
        {
            Cell newCell = ((PlayerMovedEventArgs)e).NewCell;
            Player player = ((PlayerMovedEventArgs)e).Player;

            currentCell = newCell;
            ShowCellEvent(currentCell, player);

            boardView.btnFinishTurn.Visible = true;
            boardView.RefreshAssetsBoard(players[currentPlayer].Wallet);
        }

        private void ShowCellEvent(Cell cell, Player player)
        {
            if (cell is CompanyCell)
            {
                Company company = ((CompanyCell)cell).Company;
                CompanyCellDialog dlg = new CompanyCellDialog(company, bank.ExchangeTable.StockRates[(int)company.BusinessGroup], bank.NrOfShares(company), players[currentPlayer].Wallet);
                DialogResult res = dlg.ShowDialog();
                if (res == DialogResult.OK)
                {
                    boardView.btnCellEvent.Visible = false;
                    int value = dlg.SharesBought;
                    if (value > 0)
                    {
                        player.Buy(bank, company, value);
                    }
                }
                else if(res == DialogResult.Cancel)
                {
                    boardView.btnCellEvent.Visible = true;
                }

                if(((CompanyCell)cell).IsStockMarket)
                {
                    boardView.OpenStockMarket(bank);
                }
            }
            else if (cell is FateCell)
            {
                drawFateCard();
            }
            else if (cell is StockMarketCell || cell is AuctionCell)
            {
                boardView.OpenStockMarket(bank);
            }
        }

        private void initializeNewGame(int nrOfPlayers)
        {
            this.nrOfPlayers = nrOfPlayers;
            players = new Player[nrOfPlayers];
            for (int i = 0; i < players.Length; i++)
            {
                players[i] = new Player("Player " + (i + 1), Constants.PLAYER_STARTING_MONEY);
                players[i].ShortOnFunds += Player_ShortOnFunds;
                players[i].Bankruptcy += Player_Bankruptcy;
            }

            Dictionary<Company, List<CompanyCard>> companyCards = new Dictionary<Company, List<CompanyCard>>();
            for (int i = 0; i < Constants.COMPANIES.Length; i++)
            {
                List<CompanyCard> list = new List<CompanyCard>(Constants.NR_COMPANY_SHARES);
                for (int j = 0; j < Constants.NR_COMPANY_SHARES; j++)
                {
                    list.Add(new CompanyCard(Constants.COMPANIES[i]));
                }
                companyCards[Constants.COMPANIES[i]] = list;
            }

            Dictionary<PrivateCompany, PrivateCompanyCard> privateCompanyCards = new Dictionary<PrivateCompany, PrivateCompanyCard>();
            for (int i = 0; i < Constants.PRIVATE_COMPANIES.Length; i++)
            {
                privateCompanyCards[Constants.PRIVATE_COMPANIES[i]] = new PrivateCompanyCard(Constants.PRIVATE_COMPANIES[i]);
            }

            List<BankCard> bankCards = new List<BankCard>();
            List<BondCard> bondCardsA = new List<BondCard>();
            List<BondCard> bondCardsB = new List<BondCard>();
            for (int i = 0; i < Constants.NR_OF_BANK_CARDS; i++)
            {
                bankCards.Add(new BankCard(Constants.BANK_CARD_COST, true));
            }
            for (int i = 0; i < Constants.NR_OF_BOND_CARDS[0]; i++)
            {
                bondCardsA.Add(new BondCard(Constants.BOND_CARD_COST[(int)BondCardGroup.Purple], BondCardGroup.Purple));
            }
            for (int i = 0; i < Constants.NR_OF_BOND_CARDS[1]; i++)
            {
                bondCardsB.Add(new BondCard(Constants.BOND_CARD_COST[(int)BondCardGroup.Pink], BondCardGroup.Pink));
            }
            List<BondCard>[] bondCards = { bondCardsA, bondCardsB };

            List<FundLetterCard>[,] fundLetterCards = new List<FundLetterCard>[2, 3];
            fundLetterCards[(int)FundLetterCardGroup.Pink, 0] = new List<FundLetterCard>(Constants.NR_OF_FUND_LETTER_CARDS_PER_GROUP);
            for (int i = 0; i < Constants.NR_OF_FUND_LETTER_CARDS_PER_GROUP; i++)
            {
                fundLetterCards[(int)FundLetterCardGroup.Pink, 0].Add(new FundLetterCard("Sjóðsbréf 4", "Verðbréfamarkaður Íslandsbanka", 1000, FundLetterCardGroup.Pink, 0));
            }

            fundLetterCards[(int)FundLetterCardGroup.Pink, 1] = new List<FundLetterCard>(Constants.NR_OF_FUND_LETTER_CARDS_PER_GROUP);
            for (int i = 0; i < Constants.NR_OF_FUND_LETTER_CARDS_PER_GROUP; i++)
            {
                fundLetterCards[(int)FundLetterCardGroup.Pink, 1].Add(new FundLetterCard("Sjóðsbréf 2", "Verðbréfamarkaður Íslandsbanka", 2000, FundLetterCardGroup.Pink, 1));
            }

            fundLetterCards[(int)FundLetterCardGroup.Pink, 2] = new List<FundLetterCard>(Constants.NR_OF_FUND_LETTER_CARDS_PER_GROUP);
            for (int i = 0; i < Constants.NR_OF_FUND_LETTER_CARDS_PER_GROUP; i++)
            {
                fundLetterCards[(int)FundLetterCardGroup.Pink, 2].Add(new FundLetterCard("Skuldabréf", "Glitnir h.f", 3000, FundLetterCardGroup.Pink, 3));
            }

            fundLetterCards[(int)FundLetterCardGroup.Gray, 0] = new List<FundLetterCard>(Constants.NR_OF_FUND_LETTER_CARDS_PER_GROUP);
            for (int i = 0; i < Constants.NR_OF_FUND_LETTER_CARDS_PER_GROUP; i++)
            {
                fundLetterCards[(int)FundLetterCardGroup.Gray, 0].Add(new FundLetterCard("Sjóðsbréf 3", "Verðbréfamarkaður Íslandsbanka", 4000, FundLetterCardGroup.Gray, 0));
            }

            fundLetterCards[(int)FundLetterCardGroup.Gray, 1] = new List<FundLetterCard>(Constants.NR_OF_FUND_LETTER_CARDS_PER_GROUP);
            for (int i = 0; i < Constants.NR_OF_FUND_LETTER_CARDS_PER_GROUP; i++)
            {
                fundLetterCards[(int)FundLetterCardGroup.Gray, 1].Add(new FundLetterCard("Bankabréf", "Íslandsbanki", 5000, FundLetterCardGroup.Gray, 1));
            }

            fundLetterCards[(int)FundLetterCardGroup.Gray, 2] = new List<FundLetterCard>(Constants.NR_OF_FUND_LETTER_CARDS_PER_GROUP);
            for (int i = 0; i < Constants.NR_OF_FUND_LETTER_CARDS_PER_GROUP; i++)
            {
                fundLetterCards[(int)FundLetterCardGroup.Gray, 2].Add(new FundLetterCard("Sjóðsbréf 1", "Verðbréfamarkaður Íslandsbanka", 6000, FundLetterCardGroup.Gray, 3));
            }

            List<CarCard> carCards = new List<CarCard>();
            for (int i = 0; i < Constants.CARS.Length; i++)
            {
                int count;
                switch (i)
                {
                    case 0:
                        count = 6;
                        break;
                    case 3:
                        count = 2;
                        break;
                    case 6:
                        count = 2;
                        break;
                    default:
                        count = 1;
                        break;
                }
                for (int j = 0; j < count; j++)
                {
                    carCards.Add(new CarCard(Constants.CARS[i]));
                }
            }

            // Create the exchange rate deck


            bank = new Bank(400000, bankCards, bondCards, companyCards, privateCompanyCards, fundLetterCards, carCards);
            bank.ShortOnFunds += Bank_ShortOnFunds;
            for (int i = 0; i < players.Length; i++)
            {
                bank.Pay(10000, players[i]);
            }
            boardView.Init(players, bank);
            currentPlayer = 0;
            //players[currentPlayer] = new Player("Debug player", 400000, bankCards, bondCards, companyCards, privateCompanyCards, fundLetterCards, carCards);
            // Create the fate card deck

            ShuffleFateCards();
            ShuffleGovernmentCards();

            //walletView.Clear();
            //walletView.layoutCompanyCards(bank.Wallet);
            ChangeView(boardView);

            //cells[0] = new PrivateCompanyCell(priva)
        }

        private void ShuffleFateCards()
        {
            fateCardDeck = new List<FateCard>();
            for (int i = 0; i < Constants.FATE_CARD_TEXTS.Length; i++)
            {
                fateCardDeck.Add(new FateCard(Constants.FATE_CARD_TEXTS[i]));
            }
            fateCardDeck.Shuffle();
        }

        private void ShuffleGovernmentCards()
        {
            governmentCardDeck = new List<GovernmentCard>();
            for (int i = 0; i < Constants.GOVERNMENT_CARD_TEXTS.Length; i++)
            {
                governmentCardDeck.Add(new GovernmentCard(Constants.GOVERNMENT_CARD_TEXTS[i]));
            }
            governmentCardDeck.Shuffle();
        }

        private void ChangeView(Control newView)
        {
            if (currentView != null)
                this.Controls.Remove(currentView);
            this.Controls.Add(newView);
            currentView = newView;
        }

        private void Cell_Click(object sender, EventArgs e)
        {
            MessageBox.Show(sender.GetType().FullName);
        }

        private void Player_Bankruptcy(object sender, EventArgs e)
        {
            Player player = (Player)sender;
            MessageBox.Show(player.Name + " has gone bankrupt!");
        }

        private void Bank_ShortOnFunds(object sender, EventArgs e)
        {
            MessageBox.Show("The bank has gone bankrupt!");
            continueDebugging = false;
        }

        private void Player_ShortOnFunds(object sender, EventArgs e)
        {
            Player player = (Player)sender;
            int dueAmount = ((ShortOnFundsEventArgs)e).DueAmount;
            Player debtee = ((ShortOnFundsEventArgs)e).Debtee;
            bool debtToBank = ((ShortOnFundsEventArgs)e).DebtToBank;
            Console.WriteLine(player.Name + " is short on funds, he owes: " + dueAmount + " to " + (debtee == null ? (debtToBank ? " the bank" : " to unknown entity") : player.Name));
            // Check for any assets
            BondCard bondCard = player.Wallet.BondCards[(int)BondCardGroup.Purple].First();
            if (bondCard != null)
            {
                player.Give(bondCard, bank);
                bank.Pay(bondCard.Amount, player);
            }
            continueDebugging = false;
        }

        private void drawFateCard()
        {
            FateCard card = fateCardDeck[0];
            fateCardDeck.Remove(card);
            if(debugMode)
            {
                Console.WriteLine("Funds: " + players[currentPlayer].Wallet.Cash);
            }

            activateFateCard(fateCardDeck.FirstOrDefault(x => x.CardId == 1));

            if (fateCardDeck.Count == 0)
                ShuffleFateCards();
        }

        private void activateFateCard(FateCard card)
        {
            GenericDialog.ShowDialog(card.Text, MessageBoxButtons.OK);
            switch (card.CardId)
            {
                case 0:
                    players[currentPlayer].Pay(1000, bank);
                    break;
                case 1:
                    players[currentPlayer].Pay(1500, bank);
                    break;
                case 2:
                    int playerToTheLeft = currentPlayer - 1;
                    if (playerToTheLeft < 0)
                        playerToTheLeft = nrOfPlayers - 1;
                    players[currentPlayer].Pay(200, players[playerToTheLeft]);
                    players[playerToTheLeft].MissesNextTurn = true;
                    Constants.CompanyDictionary[Constants.CompanyNamesEnum.Flugleidir].Profit += players[currentPlayer].Pay(200);
                    break;
                case 3:
                    players[currentPlayer].Pay(50, bank);
                    break;
                case 4:
                    players[currentPlayer].Pay(200, bank);
                    break;
                case 5:
                    Constants.CompanyDictionary[Constants.CompanyNamesEnum.UrvalUtsyn].Profit += players[currentPlayer].Pay(400);
                    players[currentPlayer].MissesNextTurn = true;
                    break;
                case 6:
                    players[currentPlayer].Pay(350, bank);
                    PayBondInterest();
                    break;
                case 7:
                    for (int i = 0; i < players.Length; i++)
                    {
                        players[i].Pay(50 * players[i].NrOfCars(), bank);
                    }
                    PayBondInterest();
                    break;
                case 8:
                    players[currentPlayer].Pay(300, bank);
                    PayBondInterest();
                    break;
                case 9:
                    players[currentPlayer].Pay(400, bank);
                    break;
                case 10:
                    players[currentPlayer].Pay(50, bank);
                    break;
                case 11:
                    bank.Pay(1000, players[currentPlayer]);
                    PayBondInterest();
                    break;
                case 12:
                    // Fara á reit búnaðarbankans
                    break;
                case 13:
                    players[currentPlayer].Pay(800, bank);
                    PayBondInterest();
                    break;
                case 14:
                    // Draga annað gluggaumslag og afhenda öðrum spilara
                    break;
                case 15:
                    // Fara á reit íslandsbanka
                    break;
                case 16:
                    players[currentPlayer].Pay(2000, bank);
                    break;
                case 17:
                    players[currentPlayer].Pay(500, bank);
                    break;
                case 18:
                    List<CarCard> carCards;
                    for (int i = 0; i < players.Length; i++)
                    {
                        bool missesTurn = true;
                        carCards = players[i].GetCarCards();
                        foreach (CarCard carCard in carCards)
                        {
                            if (carCard.Car.Category == CarCategory.SUV)
                                missesTurn = false;
                        }
                        players[i].MissesNextTurn = missesTurn;
                    }
                    break;
                case 19:
                    players[currentPlayer].Pay(50, bank);
                    break;
                case 20:
                    int nrOfCompaniesOwned = 0;
                    for (int i = 0; i < Constants.COMPANIES.Length; i++)
                    {
                        if (players[currentPlayer].NrOfShares(Constants.COMPANIES[i]) > 0)
                            nrOfCompaniesOwned++;
                    }
                    nrOfCompaniesOwned += players[currentPlayer].NrOfPrivateCompanies();
                    int totalOwed = 100 * nrOfCompaniesOwned;
                    players[currentPlayer].Pay(totalOwed, bank);
                    PayBondInterest();
                    break;
                case 21:
                    players[currentPlayer].Pay(400, bank);
                    break;
                case 22:
                    players[currentPlayer].Pay(1000, bank);
                    PayBondInterest();
                    break;
                case 23:
                    nrOfCompaniesOwned = 0;
                    for (int i = 0; i < Constants.COMPANIES.Length; i++)
                    {
                        if (players[currentPlayer].NrOfShares(Constants.COMPANIES[i]) > 0)
                            nrOfCompaniesOwned++;
                    }
                    nrOfCompaniesOwned += players[currentPlayer].NrOfPrivateCompanies();
                    totalOwed = 100 * nrOfCompaniesOwned;
                    Constants.CompanyDictionary[Constants.CompanyNamesEnum.UrvalUtsyn].Profit += players[currentPlayer].Pay(totalOwed);
                    break;
                case 24:
                    PayBondInterest();
                    break;
                case 25:
                    players[currentPlayer].Pay(500, bank);
                    break;
                case 26:
                    nrOfCompaniesOwned = 0;
                    for (int i = 0; i < Constants.COMPANIES.Length; i++)
                    {
                        if (players[currentPlayer].NrOfShares(Constants.COMPANIES[i]) >= (int)(Constants.NR_COMPANY_SHARES * 0.75))
                            nrOfCompaniesOwned++;
                    }
                    nrOfCompaniesOwned += players[currentPlayer].NrOfPrivateCompanies();
                    totalOwed = 200 * nrOfCompaniesOwned;
                    players[currentPlayer].Pay(totalOwed, bank);
                    break;
                case 27:
                    nrOfCompaniesOwned = 0;
                    for (int i = 0; i < Constants.COMPANIES.Length; i++)
                    {
                        if (players[currentPlayer].NrOfShares(Constants.COMPANIES[i]) >= (int)(Constants.NR_COMPANY_SHARES * 0.5))
                            nrOfCompaniesOwned++;
                    }
                    nrOfCompaniesOwned += players[currentPlayer].NrOfPrivateCompanies();
                    totalOwed = 500 * nrOfCompaniesOwned;
                    players[currentPlayer].Pay(totalOwed, bank);
                    PayBondInterest();
                    break;
                case 28:
                    // Borga 10.000 núna eða á næsta verðbréfareit með vöxtum
                    break;
                case 29:
                    players[currentPlayer].Pay(1000, bank);
                    break;
                case 30:
                    if (players[currentPlayer].HasShare(Constants.CompanyDictionary[Constants.CompanyNamesEnum.Glitnir]))
                    {
                        for (int i = 0; i < players.Length; i++)
                        {
                            if (i != currentPlayer)
                            {
                                players[i].Pay(1000, players[currentPlayer]);
                            }
                        }
                    }
                    break;
                case 31:
                    players[currentPlayer].Pay(300, bank);
                    PayBondInterest();
                    break;
                case 32:
                    players[currentPlayer].Pay(2000, bank);
                    PayBondInterest();
                    break;
                case 33:
                    // Aftur á bak um þrjá reiti
                    PayBondInterest();
                    break;
                case 34:
                    players[currentPlayer].Pay(5000, bank);
                    PayBondInterest();
                    break;
                case 35:
                    // Ferð á næsta VÍB
                    break;
                case 36:
                    carCards = players[currentPlayer].GetCarCards();
                    foreach (CarCard carCard in carCards)
                    {
                        players[currentPlayer].Give(carCard, bank);
                    }
                    break;
                case 37:
                    // Ferð á reit skeljungs
                    break;
                case 38:
                    nrOfCompaniesOwned = 0;
                    for (int i = 0; i < Constants.COMPANIES.Length; i++)
                    {
                        if (players[currentPlayer].NrOfShares(Constants.COMPANIES[i]) >= (int)(Constants.NR_COMPANY_SHARES * 0.5))
                            nrOfCompaniesOwned++;
                    }
                    nrOfCompaniesOwned += players[currentPlayer].NrOfPrivateCompanies();
                    totalOwed = 100 * nrOfCompaniesOwned;
                    players[currentPlayer].Pay(totalOwed, bank);
                    break;
                case 39:
                    int collectedFunds = 0;
                    for (int i = 0; i < players.Length; i++)
                    {
                        collectedFunds += players[i].Pay(800);
                    }
                    Constants.CompanyDictionary[Constants.CompanyNamesEnum.Skeljungur].Profit += (collectedFunds / 2);
                    Constants.CompanyDictionary[Constants.CompanyNamesEnum.Esso].Profit += (collectedFunds / 2);
                    break;
                case 40:
                    players[currentPlayer].Pay(50, bank);
                    break;
                case 41:
                    int nrOfStaff = 0;
                    for (int i = 0; i < Constants.COMPANIES.Length; i++)
                    {
                        nrOfStaff += Constants.COMPANIES[i].NrOfStaff * (players[currentPlayer].NrOfShares(Constants.COMPANIES[i]) / Constants.NR_COMPANY_SHARES);
                    }
                    players[currentPlayer].Pay(nrOfStaff * 2, bank);
                    break;
                case 42:
                    players[currentPlayer].MissesNextTurn = true;
                    PayBondInterest();
                    break;
                case 43:
                    players[currentPlayer].Pay(100, bank);
                    break;
                case 44:
                    totalOwed = (int)(players[currentPlayer].TotalAssets * 0.05);
                    players[currentPlayer].Pay(totalOwed, bank);
                    break;
                case 45:
                    // Býður spilafélaga í heimsókn í eitt af fyrirtækjum þínum
                    PayBondInterest();
                    break;
                case 46:
                    // Ferð á næsta uppboðsreit
                    break;
                case 47:
                    Car carTobuy = bank.Wallet.CarCards.Find(o => o.Car.Category == CarCategory.SUV).Car; // Todo breyta hvernig bílar eru sóttir frá banka
                    if (carTobuy != null)
                    {
                        players[currentPlayer].Buy(bank, carTobuy);
                        players[currentPlayer].Pay(800, bank);
                    }
                    break;
                case 48:
                    // Kaupa ódýrasta bílinn sem er til
                    PayBondInterest();
                    break;
                case 49:
                    totalOwed = players[currentPlayer].Pay(500);
                    Constants.CompanyDictionary[Constants.CompanyNamesEnum.Kodak].Profit += totalOwed;
                    break;
                case 50:
                    players[currentPlayer].Pay(700, bank);
                    break;
                case 51:
                    players[currentPlayer].Pay(50, bank);
                    break;
                case 52:
                    players[currentPlayer].Pay(700, bank);
                    PayBondInterest();
                    break;
                case 53:
                    nrOfCompaniesOwned = 0;
                    for (int i = 0; i < Constants.COMPANIES.Length; i++)
                    {
                        if (players[currentPlayer].NrOfShares(Constants.COMPANIES[i]) >= (int)(Constants.NR_COMPANY_SHARES * 0.5))
                            nrOfCompaniesOwned++;
                    }
                    nrOfCompaniesOwned += players[currentPlayer].NrOfPrivateCompanies();
                    totalOwed = 500 * nrOfCompaniesOwned;
                    players[currentPlayer].Pay(totalOwed, bank);
                    break;
                case 54:
                    totalOwed = players[currentPlayer].Pay(200);
                    Constants.CompanyDictionary[Constants.CompanyNamesEnum.Oddi].Profit += totalOwed;
                    break;
                case 55:
                    players[currentPlayer].Pay(1200, bank);
                    break;
                case 56:
                    players[currentPlayer].Pay(500, bank);
                    break;
                case 57:
                    totalOwed = players[currentPlayer].Pay(200);
                    Constants.CompanyDictionary[Constants.CompanyNamesEnum.Radiobudin].Profit += totalOwed;
                    break;
                case 58:
                    int playerToTheRight = currentPlayer %= (currentPlayer + 1);
                    players[playerToTheRight].Pay(700, players[currentPlayer]);
                    break;
                case 59:
                    totalOwed = players[currentPlayer].Pay(200);
                    Constants.CompanyDictionary[Constants.CompanyNamesEnum.Penninn].Profit += totalOwed;
                    break;
            }
        }
        private void PayBondInterest()
        {
            for (int i = 0; i < players.Length; i++)
            {
                int totalInterestAmount = 0;
                foreach (BondCard bond in players[i].Wallet.BondCards[(int)BondCardGroup.Pink])
                {
                    totalInterestAmount += (int)(bond.Amount * bank.ExchangeTable.InterestRate);
                }
                foreach (BondCard bond in players[i].Wallet.BondCards[(int)BondCardGroup.Purple])
                {
                    totalInterestAmount += (int)(bond.Amount * bank.ExchangeTable.InterestRate);
                }
            }
        }

        private void Game_Shown(object sender, EventArgs e)
        {
            initializeNewGame(1);
            //foreach (CompanyCard card in bank.Wallet.GetCompanyCardList())
            //{
            //    bank.GiveCard(card, players[0]);
            //}
            //boardView.OpenStockMarket(bank);
        }
    }
}
