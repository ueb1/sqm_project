﻿namespace Game.Controls
{
    partial class ExchangeRateTableControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblGroupRate1 = new System.Windows.Forms.Label();
            this.lblGroupRate3 = new System.Windows.Forms.Label();
            this.lblGroupRate5 = new System.Windows.Forms.Label();
            this.lblGroupRate2 = new System.Windows.Forms.Label();
            this.lblGroupRate4 = new System.Windows.Forms.Label();
            this.lblGroupRate6 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lblFundGroupRate1 = new System.Windows.Forms.Label();
            this.lblFundGroupRate2 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.lblInterestRates = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LimeGreen;
            this.panel1.Controls.Add(this.lblGroupRate1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(6, 16);
            this.panel1.Margin = new System.Windows.Forms.Padding(6, 16, 6, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(131, 42);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Magenta;
            this.panel2.Controls.Add(this.lblGroupRate3);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Location = new System.Drawing.Point(6, 58);
            this.panel2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(131, 42);
            this.panel2.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.SlateBlue;
            this.panel3.Controls.Add(this.lblGroupRate5);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Location = new System.Drawing.Point(6, 100);
            this.panel3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(131, 42);
            this.panel3.TabIndex = 2;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.lblFundGroupRate1);
            this.panel4.Controls.Add(this.label15);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Controls.Add(this.label13);
            this.panel4.Location = new System.Drawing.Point(6, 142);
            this.panel4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(131, 42);
            this.panel4.TabIndex = 2;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.DarkOrange;
            this.panel5.Controls.Add(this.lblGroupRate2);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Location = new System.Drawing.Point(143, 16);
            this.panel5.Margin = new System.Windows.Forms.Padding(6, 16, 6, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(131, 42);
            this.panel5.TabIndex = 1;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.NavajoWhite;
            this.panel6.Controls.Add(this.lblGroupRate4);
            this.panel6.Controls.Add(this.label4);
            this.panel6.Location = new System.Drawing.Point(143, 58);
            this.panel6.Margin = new System.Windows.Forms.Padding(6, 16, 6, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(131, 42);
            this.panel6.TabIndex = 3;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Teal;
            this.panel7.Controls.Add(this.lblGroupRate6);
            this.panel7.Controls.Add(this.label6);
            this.panel7.Location = new System.Drawing.Point(143, 100);
            this.panel7.Margin = new System.Windows.Forms.Padding(6, 16, 6, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(131, 42);
            this.panel7.TabIndex = 1;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Silver;
            this.panel8.Controls.Add(this.label18);
            this.panel8.Controls.Add(this.lblFundGroupRate2);
            this.panel8.Controls.Add(this.label19);
            this.panel8.Controls.Add(this.label20);
            this.panel8.Location = new System.Drawing.Point(143, 142);
            this.panel8.Margin = new System.Windows.Forms.Padding(6, 16, 6, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(131, 42);
            this.panel8.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 42);
            this.label1.TabIndex = 0;
            this.label1.Text = "500";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 42);
            this.label2.TabIndex = 1;
            this.label2.Text = "1000";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 42);
            this.label3.TabIndex = 2;
            this.label3.Text = "1500";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(3, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 42);
            this.label4.TabIndex = 3;
            this.label4.Text = "2000";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(3, 0);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 42);
            this.label5.TabIndex = 4;
            this.label5.Text = "2500";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(3, 0);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 42);
            this.label6.TabIndex = 5;
            this.label6.Text = "3000";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblGroupRate1
            // 
            this.lblGroupRate1.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGroupRate1.ForeColor = System.Drawing.Color.Black;
            this.lblGroupRate1.Location = new System.Drawing.Point(95, 0);
            this.lblGroupRate1.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.lblGroupRate1.Name = "lblGroupRate1";
            this.lblGroupRate1.Size = new System.Drawing.Size(39, 42);
            this.lblGroupRate1.TabIndex = 1;
            this.lblGroupRate1.Text = "1";
            this.lblGroupRate1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblGroupRate3
            // 
            this.lblGroupRate3.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGroupRate3.ForeColor = System.Drawing.Color.Black;
            this.lblGroupRate3.Location = new System.Drawing.Point(95, 0);
            this.lblGroupRate3.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.lblGroupRate3.Name = "lblGroupRate3";
            this.lblGroupRate3.Size = new System.Drawing.Size(39, 42);
            this.lblGroupRate3.TabIndex = 2;
            this.lblGroupRate3.Text = "1";
            this.lblGroupRate3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblGroupRate5
            // 
            this.lblGroupRate5.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGroupRate5.ForeColor = System.Drawing.Color.Black;
            this.lblGroupRate5.Location = new System.Drawing.Point(95, 0);
            this.lblGroupRate5.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.lblGroupRate5.Name = "lblGroupRate5";
            this.lblGroupRate5.Size = new System.Drawing.Size(39, 42);
            this.lblGroupRate5.TabIndex = 3;
            this.lblGroupRate5.Text = "1";
            this.lblGroupRate5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblGroupRate2
            // 
            this.lblGroupRate2.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGroupRate2.ForeColor = System.Drawing.Color.Black;
            this.lblGroupRate2.Location = new System.Drawing.Point(95, 0);
            this.lblGroupRate2.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.lblGroupRate2.Name = "lblGroupRate2";
            this.lblGroupRate2.Size = new System.Drawing.Size(39, 42);
            this.lblGroupRate2.TabIndex = 5;
            this.lblGroupRate2.Text = "1";
            this.lblGroupRate2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblGroupRate4
            // 
            this.lblGroupRate4.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGroupRate4.ForeColor = System.Drawing.Color.Black;
            this.lblGroupRate4.Location = new System.Drawing.Point(95, 0);
            this.lblGroupRate4.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.lblGroupRate4.Name = "lblGroupRate4";
            this.lblGroupRate4.Size = new System.Drawing.Size(39, 42);
            this.lblGroupRate4.TabIndex = 6;
            this.lblGroupRate4.Text = "1";
            this.lblGroupRate4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblGroupRate6
            // 
            this.lblGroupRate6.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGroupRate6.ForeColor = System.Drawing.Color.Black;
            this.lblGroupRate6.Location = new System.Drawing.Point(95, 0);
            this.lblGroupRate6.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.lblGroupRate6.Name = "lblGroupRate6";
            this.lblGroupRate6.Size = new System.Drawing.Size(39, 42);
            this.lblGroupRate6.TabIndex = 7;
            this.lblGroupRate6.Text = "1";
            this.lblGroupRate6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(3, 5);
            this.label13.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 8);
            this.label13.TabIndex = 0;
            this.label13.Text = "Sjóðsbréf  4";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(3, 17);
            this.label14.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 8);
            this.label14.TabIndex = 1;
            this.label14.Text = "Skuldabréf Glitnis";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(3, 29);
            this.label15.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(65, 8);
            this.label15.TabIndex = 2;
            this.label15.Text = "Sjóðsbréf  3";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFundGroupRate1
            // 
            this.lblFundGroupRate1.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFundGroupRate1.ForeColor = System.Drawing.Color.Black;
            this.lblFundGroupRate1.Location = new System.Drawing.Point(95, 0);
            this.lblFundGroupRate1.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.lblFundGroupRate1.Name = "lblFundGroupRate1";
            this.lblFundGroupRate1.Size = new System.Drawing.Size(39, 42);
            this.lblFundGroupRate1.TabIndex = 5;
            this.lblFundGroupRate1.Text = "1";
            this.lblFundGroupRate1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblFundGroupRate2
            // 
            this.lblFundGroupRate2.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFundGroupRate2.ForeColor = System.Drawing.Color.Black;
            this.lblFundGroupRate2.Location = new System.Drawing.Point(95, 0);
            this.lblFundGroupRate2.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.lblFundGroupRate2.Name = "lblFundGroupRate2";
            this.lblFundGroupRate2.Size = new System.Drawing.Size(39, 42);
            this.lblFundGroupRate2.TabIndex = 6;
            this.lblFundGroupRate2.Text = "1";
            this.lblFundGroupRate2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(3, 29);
            this.label18.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(65, 8);
            this.label18.TabIndex = 8;
            this.label18.Text = "Sjóðsbréf  1";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(3, 17);
            this.label19.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(83, 8);
            this.label19.TabIndex = 7;
            this.label19.Text = "Bankabréf Íslandsbanka";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(3, 5);
            this.label20.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(65, 8);
            this.label20.TabIndex = 6;
            this.label20.Text = "Sjóðsbréf  2";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblInterestRates
            // 
            this.lblInterestRates.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInterestRates.Location = new System.Drawing.Point(6, 184);
            this.lblInterestRates.Name = "lblInterestRates";
            this.lblInterestRates.Size = new System.Drawing.Size(268, 16);
            this.lblInterestRates.TabIndex = 4;
            this.lblInterestRates.Text = "Vextir: 10%";
            this.lblInterestRates.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(6, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(268, 16);
            this.label8.TabIndex = 5;
            this.label8.Text = "Gengistafla";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // ExchangeRateTableControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lblInterestRates);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "ExchangeRateTableControl";
            this.Size = new System.Drawing.Size(280, 200);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblGroupRate1;
        private System.Windows.Forms.Label lblGroupRate3;
        private System.Windows.Forms.Label lblGroupRate5;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblGroupRate2;
        private System.Windows.Forms.Label lblGroupRate4;
        private System.Windows.Forms.Label lblGroupRate6;
        private System.Windows.Forms.Label lblFundGroupRate1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label lblFundGroupRate2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label lblInterestRates;
        private System.Windows.Forms.Label label8;
    }
}
