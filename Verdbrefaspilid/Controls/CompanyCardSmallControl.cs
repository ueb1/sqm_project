﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Game.Entities.Cards;
using Game.Entities;
using Game.Dialogs;

namespace Game.Controls
{
    public partial class CompanyCardSmallControl : UserControl
    {
        private int sizeOfStack;
        private Company company;

        public CompanyCardSmallControl(Company company, int sizeOfStack)
        {
            InitializeComponent();
            this.sizeOfStack = sizeOfStack;
            this.company = company;
            if (sizeOfStack > 1)
            {
                int padDown = (sizeOfStack - 1) * 4;
                this.Height += padDown;
                pnlCard.Location = new Point(pnlCard.Location.X, pnlCard.Location.Y + padDown);                       
            }

            picImage.Image = company.ImageSmall;
            lblCost.Text = "Nafnverð: " + company.CostEachShare;
            lblEmployeeCount.Text = "Starfsmannafjöldi: " + (int)(company.NrOfStaff / Constants.NR_COMPANY_SHARES);
            lblDividend1.Text = Constants.COMPANY_DIVIDENDS[(int)company.BusinessGroup, 0].ToString();
            lblDividend2.Text = Constants.COMPANY_DIVIDENDS[(int)company.BusinessGroup, 1].ToString();
            lblDividend3.Text = Constants.COMPANY_DIVIDENDS[(int)company.BusinessGroup, 2].ToString();
            lblDividend4.Text = Constants.COMPANY_DIVIDENDS[(int)company.BusinessGroup, 3].ToString();
            this.BackColor = company.CompanyColor;
            pnlCard.BackColor = company.CompanyColor;
        }

        private void CompanyCardSmallControl_Paint(object sender, PaintEventArgs e)
        {
            for (int i = 1; i < sizeOfStack; i++)
            {
                e.Graphics.DrawLine(Pens.Black, 0, i * 4 - 1, this.Width, i * 4 - 1);
            }
        }

        private void pnlCard_Click(object sender, EventArgs e)
        {
            //CompanyCardDialog.ShowDialog(;
        }

        private void CompanyCardSmallControl_Load(object sender, EventArgs e)
        {
            foreach (Control c in pnlCard.Controls)
            {
                c.Click += new EventHandler(pnlCard_Click);
            }
        }
    }
}
