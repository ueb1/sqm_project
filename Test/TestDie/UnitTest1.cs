﻿using System;
using Game.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestDie
{
    [TestClass]
    public class TestDie
    {
        [TestMethod]
        public void TestRoll()
        {
            Die testDie = new Die();

            int expectedValue = testDie.Roll();

            Assert.AreEqual(expectedValue, testDie.Value, "The rolled value differs from the die value!");
        }
    }
}
